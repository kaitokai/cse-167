#ifndef _LIGHTPLAY_H_
#define _LIGHTPLAY_H_
//#include "glee.h"
#include "shader.h"
#include "main.h"
#include "matrixTransform.h"
#include "Matrix4.h"
#include "Material.h"
#include "cone.h"
#include "cubic.h"
#include "XYZParser.h"
#include "Light.h"

class LightPlay{
private:
	Group world_mt = Group();
	MatrixTransform light_mt = MatrixTransform(Matrix4::I());
	MatrixTransform rot_mt = MatrixTransform(Matrix4::I());
	MatrixTransform scl_mt = MatrixTransform(Matrix4::I());
	MatrixTransform trans_mt = MatrixTransform(Matrix4::I());
public:

	LightPlay(){
		float * ambient = new float[]{.1f, 0, 0, 1};
		float * diffuse = new float[]{1, 1, 1, 1};
		float * specular = new float[]{1, 1, 1, 1};
		float * lightPos = new float[]{1, -10, 1, 1};
		float cut_off = 10;
		float * spot_pos = new float[]{-1, -1, 0};
		//Light * l = new Light(Type::POINTV, GL_LIGHT1, ambient, diffuse, specular, lightPos, cut_off, spot_pos);

		float * ambient2 = new float[]{0.2f, 0.2f, 0.2f, 0};
		float * diffuse2 = new float[]{.1f, .5f, 1, 1};
		float * specular2 = new float[]{.1f, .5f, 1, 1};
		float * lightPos2 = new float[]{0, 0, 0, 1};
		float * spot_pos2 = new float[]{0, 3, 1};

		Light * l2 = new Light(Type::SPOT, GL_LIGHT0, ambient2, diffuse2, specular2, lightPos2, cut_off, spot_pos2);
		world_mt.addChild(light_mt);
		world_mt.addChild(rot_mt);

		rot_mt.addChild(scl_mt);
		scl_mt.addChild(trans_mt);
		//light_mt.addChild(*l);
		light_mt.addChild(*l2);		

		Matrix4 * temp;
		temp = new Matrix4();
		temp->makeTranslate(lightPos[0], lightPos[1], lightPos[2]);

//		Sphere * sp = new Sphere();
		//Cubic * cn = new Cubic();
		//MatrixTransform * t1 = new MatrixTransform(*temp);

		//glEnable(GL_COLOR_MATERIAL);
		//temp = new Matrix4();
		//temp->makeRotateX(spot_pos2[0]);
		//temp->makeRotateY_f(spot_pos2[1]);
		//temp->makeRotateZ_f(spot_pos2[2]);
		//temp->makeTranslate_f(lightPos2[0], lightPos2[1], lightPos2[2]);
		//MatrixTransform * t2 = new MatrixTransform(*temp);
		////t1->addChild(*sp);
		//t2->addChild(*cn);
		//light_mt.addChild(*t1);
		//light_mt.addChild(*t2);		
	}

	Matrix4 & getScaleMT(){
		return scl_mt.getM();
	}
	Matrix4 & getRotationMT(){
		return rot_mt.getM();
	}
	Matrix4 & getTranslationMT(){
		return trans_mt.getM();
	}
	

	void changeChild(XYZParser & g){
		scl_mt.removeAll();
		scl_mt.addChild(g);
		Matrix4 * temp = &g.getMatrix();
		rot_mt.getM() = *temp;
	}

	Matrix4 & getM(){
		return rot_mt.getM();
	}



	void draw(Matrix4 C){
		world_mt.draw(C);
	}
};

#endif