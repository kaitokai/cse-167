//////////////////////////////////
// VERTEX SHADER /////////////////
#version 120

varying float edge;
varying vec3 normal;

uniform vec4 cameraPos;

void main(){
	normal = gl_NormalMatrix * gl_Normal;
	gl_Position = ftransform();

	vec4 viewVector = cameraPos - gl_Position;

	edge = max(0, dot(normal, viewVector.xyz));

}
//--- EOD ---//