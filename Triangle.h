#ifndef _TRIANGLE_H_
#define _TRIANGLE_H_
#include "Vertex.h"

class Triangle{
private:
	Vertex pts[3]; // set three points for the triangle
public:
	Triangle(Vertex verts[3]){
		for (int i = 0; i < 3; i++){
			pts[i] = verts[i];
		}
	}

	Triangle(Vertex & p0, Vertex & p1, Vertex & p2){
		pts[0] = p0;
		pts[1] = p1;
		pts[2] = p2;
	}

	Vertex operator[](int i){
		return pts[i];
	}

	Vertex * getPointer(){
		return pts;
	}
};

#endif