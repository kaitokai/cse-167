#include "cone.h"

Cone::Cone(double base, double height) :base(base), height(height){
	this->base = base;
	this->height = height;
}

void Cone::draw(Matrix4 C){
	Geode::draw(C);
	render();
}

void Cone::render(){
	glutSolidCone(base, height, 20, 20);
}

void Cone::update(){

}
