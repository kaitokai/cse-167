#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "shader.h"
#include "main.h"

using namespace std;

namespace Globals
{
  //Cube cube;
  //House house;
  //bool spinClockwise = false;
  //bool isBall = false;
	Group root;
	Camera c2w;
	Shader * shader;
	Shader * refShader;
	Material * mat;

	bool isPointsDrawn = false;
	bool isEnvOn = false;
	bool isWireFramePatchOn = false;
	bool isWireFrameCurveOn = false;
	bool areControlPointsDrawn = false;
}

int main(int argc, char *argv[])
{
  float specular[]  = {1.0, 1.0, 1.0, 1.0};
  float shininess[] = {100.0};
  float position[]  = {0.0, 10.0, 1.0, 0.0};	// lightsource position
  
  glutInit(&argc, argv);      	      	      // initialize GLUT
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);   // open an OpenGL context with double buffering, RGB colors, and depth buffering
  glutInitWindowSize(Window::width, Window::height);      // set initial window size
  glutCreateWindow("OpenGL: CSE 167 Project");    	      // open window and set window title

  glEnable(GL_DEPTH_TEST);            	      // enable depth buffering
  glClear(GL_DEPTH_BUFFER_BIT);       	      // clear depth buffer
  glClearColor(0.0, 0.0, 0.0, 0.0);   	      // set clear color to black
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);  // set polygon drawing mode to fill front and back of each polygon
//  glDisable(GL_CULL_FACE);     // disable backface culling to render both sides of polygons
  glShadeModel(GL_SMOOTH);             	      // set shading to smooth
  glMatrixMode(GL_PROJECTION); 
  
  // Generate material properties:
  //glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular);
  //glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, shininess);
  //glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
  glLightModelf(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);
  glEnable(GL_COLOR_MATERIAL); // enable color material
  
  // Generate light source:
  //glLightfv(GL_LIGHT0, GL_POSITION, position);
  
  //--- Enable gl options
  glEnable(GL_LIGHTING);
  //glEnable(GL_LIGHT0);
  glEnable(GL_NORMALIZE);
  glEnable(GL_CULL_FACE);
	glEnable(GL_TEXTURE_2D);
	glCullFace(GL_BACK);

  // Install callback functions:
  glutDisplayFunc(Window::displayCallback);
  glutReshapeFunc(Window::reshapeCallback);
  glutIdleFunc(Window::idleCallback);
  glutKeyboardFunc(Window::keyboardCallback);
  glutSpecialFunc(Window::specKeyboardCallback);
  glutMouseFunc(Window::mouseCallback);
  glutMotionFunc(Window::motionHandler);

  // Initialize cube matrix:
  //Globals::cube.getMatrix().identity();

	//--- INITIALIZE GLOBALS
  Globals::shader = new Shader("sky.vert", "sky.frag", true);
	Globals::refShader = new Shader("reflect.vert", "reflect.frag", true);
	//--- BEGIN LOOP
	glutMainLoop();
  return 0;
}

