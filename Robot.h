#ifndef _ROBOT_H_
#define _ROBOT_H_

#include "matrixTransform.h"
#include "cubic.h"
#include "sphere.h"

//--- Class to build the robot thing
class Robot{
private:
	MatrixTransform * root;
	void helper(MatrixTransform * _root, Matrix4& trsfm, Geode & shpe);
	MatrixTransform * leftArm, * rightArm, * leftLeg, *rightLeg;
	MatrixTransform * rightKnee, * leftKnee;
public:
	enum JOINT{L_ARM, R_ARM, L_LEG, R_LEG};
	Robot();
	void build(Matrix4 C);
	void jointManip(JOINT j, Matrix4 mat);
};

#endif
