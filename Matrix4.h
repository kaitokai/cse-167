#ifndef _MATRIX4_H_
#define _MATRIX4_H_

#define M_PI 3.14159265358979323846

#include "Vector4.h"
#include "Vector3.h"

using namespace std;

class Matrix4
{
  protected:
    double m[4][4];   // matrix elements; first index is for rows, second for columns (row-major)
    
  public:
	
	// constructor
	Matrix4();
	Matrix4(double m[4][4]);
	Matrix4(double, double, double, double,
		double, double, double, double,
		double, double, double, double,
		double, double, double, double);

	// operators
    Matrix4& operator=(const Matrix4&);
	Matrix4& operator*(const Matrix4&);
	Vector4 operator*(const Vector4&);

    double* getPointer();
	void print(string);
	//*************
	Matrix4& mult(const Matrix4&);	
	Vector4 getCol(int index);


	// in place modifications
	void identity();
	//--- generate an identity matrix
	static Matrix4 I();

    void transpose();
  void makeRotateX(double);
	void makeRotateX_f(double);
	void makeRotateX_b(double);

	void makeRotateY(double);
	void makeRotateY_f(double);
	void makeRotateY_b(double);

	void makeRotateZ(double);
	void makeRotateZ_f(double);
	void makeRotateZ_b(double);

	void makeRotate(double, const Vector3&);
	void makeScale(double, double, double);
	void makeScale_f(double, double, double);
	void makeScale_b(double, double, double);
	void makeTranslate(double, double, double);
	void makeTranslate_f(double, double, double);
	void makeTranslate_b(double, double, double);

	void makeScale(const Vector3 & vector);
	void makeScale_f(const Vector3 & vector);
	void makeScale_b(const Vector3 & vector);
	void makeTranslate(const Vector3 & vector);
	void makeTranslate_f(const Vector3 & vector);
	void makeTranslate_b(const Vector3 & vector);

	void insert(int x, int y, double val);
	Matrix4 Matrix4::inverse();

	// accessors
	Vector3 getPosition() const;
	void getRotation(const Matrix4&);
	void getTranslation(const Matrix4&);
};

#endif