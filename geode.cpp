#include "geode.h"

void Geode::draw(Matrix4 C){
	glMatrixMode(GL_MODELVIEW);
	Matrix4 _temp = C;
	_temp.transpose();
	glLoadMatrixd(_temp.getPointer());
}

//--- EOD ---//