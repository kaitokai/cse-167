#ifndef _CUBIC_H_
#define _CUBIC_H_

#include "geode.h"

//--- creates a solid cubic shape
class Cubic : public Geode{
private:
	double size;
public:
	//--- constructor
	Cubic();
	Cubic(double size);

	~Cubic();

	//--- routines
	void draw(Matrix4 C);
	void update();

	void render();

};

#endif
