#ifndef _GROUP_H_
#define _GROUP_H_

#include <list>

#include "node.h"

using namespace std;

class Group : public Node{
private:
protected:
	list<Node*> children;

public:
	//Group(char * name, Matrix4 m2w);
	void draw(Matrix4 C);
	void update();
	bool addChild(Node& child);
	bool removeChild(Node& child);
	bool removeAll();
};

#endif