#ifndef _HOUSE_H_
#define _HOUSE_H_
#include <iostream>
//#include <GL\glew.h>

#include <GL\glut.h>
#include "Vector3.h"
#include "Vector4.h"

class House{
private:
	Vector3 * verts;
	Vector3 * clrs;
	Vector4 * colors_a;

public:
	House();
	void render();
	Vector3 * getVertsPtr();
	Vector3 * getColorPtr();

	double * vertz;
	double * colrz;

};
#endif
