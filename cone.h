#ifndef _CONE_H_
#define _CONE_H_

#include "geode.h"
#include <gl/glut.h>
class Cone : public Geode{
private:
	double base;
	double height;
public:
	Cone(double base, double height);
	Cone():Cone(1, 1){}

	void draw(Matrix4 C);

	void render();

	void update();


};

#endif