//#include "Cube.h"
//#include "Matrix4.h"
//#include "window.h"
//#include "time.h"
//#include <iostream>
//#include <limits>
//
//using namespace std;
//
//
//
//Cube::Cube()
//{
//	resetBall();
//	radius = 2.0;
//	decay = 0.9;
//}
//
//void Cube::updatePosition()
//{
//	velocity = velocity + gravity;
//	if (velocity.z() < -1)	{
//		velocity = Vector3(velocity.x(), velocity.y(), -1);
//	}
//	translate(velocity);
//	checkCollision();
//	Vector3 position = model2world.getPosition();
//}
//
//Matrix4& Cube::getMatrix()
//{
//  return model2world;
//}
//
//void Cube::spin(double deg)   // deg is in degrees
//{
//	Matrix4 r;
//	r.makeRotateY(deg);
//
//	model2world = model2world * r;
//}
//
//void Cube::orbit(double deg)   // deg is in degrees
//{
//	Matrix4 o;
//	o.makeRotateZ(deg);
//
//	model2world = o * model2world;
//}
//
//void Cube::scale(double factor)
//{
//	Matrix4 s;
//	s.makeScale(factor, factor, factor);
//
//	model2world = model2world * s;
//}
//
//void Cube::translate(Vector3 offset)
//{
//	Matrix4 t;
//	t.makeTranslate(offset.x(), offset.y(), offset.z());
//
//	model2world = t * model2world;
//}
//
//void Cube::reset()
//{
//	model2world.identity();
//	resetBall();
//}
//
//void Cube::printPosition(string message) const
//{
//	model2world.getPosition().print(message);
//}
//
//void Cube::draw(bool drawBall) const
//{
//	// Tell OpenGL what ModelView matrix to use:
//	Matrix4 glmatrix;
//	glmatrix = model2world;
//	glmatrix.transpose();
//	glLoadMatrixd(glmatrix.getPointer());
//
//
//	if (drawBall) {
//		glutSolidSphere(radius, 32, 32);
//	}
//	else {
//		// Draw all six faces of the cube:
//		glBegin(GL_QUADS);
//		glColor3f(0.0, 1.0, 0.0);		// This makes the cube green; the parameters are for red, green and blue. 
//		// To change the color of the other faces you will need to repeat this call before each face is drawn.
//		// Draw front face:
//		glNormal3f(0.0, 0.0, 1.0);
//		glVertex3f(-5.0, 5.0, 5.0);
//		glVertex3f(5.0, 5.0, 5.0);
//		glVertex3f(5.0, -5.0, 5.0);
//		glVertex3f(-5.0, -5.0, 5.0);
//
//		// Draw left side:
//		glNormal3f(-1.0, 0.0, 0.0);
//		glVertex3f(-5.0, 5.0, 5.0);
//		glVertex3f(-5.0, 5.0, -5.0);
//		glVertex3f(-5.0, -5.0, -5.0);
//		glVertex3f(-5.0, -5.0, 5.0);
//
//		// Draw right side:
//		glNormal3f(1.0, 0.0, 0.0);
//		glVertex3f(5.0, 5.0, 5.0);
//		glVertex3f(5.0, 5.0, -5.0);
//		glVertex3f(5.0, -5.0, -5.0);
//		glVertex3f(5.0, -5.0, 5.0);
//
//		// Draw back face:
//		glNormal3f(0.0, 0.0, -1.0);
//		glVertex3f(-5.0, 5.0, -5.0);
//		glVertex3f(5.0, 5.0, -5.0);
//		glVertex3f(5.0, -5.0, -5.0);
//		glVertex3f(-5.0, -5.0, -5.0);
//
//		// Draw top side:
//		glNormal3f(0.0, 1.0, 0.0);
//		glVertex3f(-5.0, 5.0, 5.0);
//		glVertex3f(5.0, 5.0, 5.0);
//		glVertex3f(5.0, 5.0, -5.0);
//		glVertex3f(-5.0, 5.0, -5.0);
//
//		// Draw bottom side:
//		glNormal3f(0.0, -1.0, 0.0);
//		glVertex3f(-5.0, -5.0, -5.0);
//		glVertex3f(5.0, -5.0, -5.0);
//		glVertex3f(5.0, -5.0, 5.0);
//		glVertex3f(-5.0, -5.0, 5.0);
//		glEnd();
//	}
//}
//
//void Cube::checkCollision() {
//	Vector3 position = model2world.getPosition();
//	double x = position.x();
//	double y = position.y();
//	double z = position.z();
//
//	// used for proejction
//	GLint viewport[4];
//	glGetIntegerv(GL_VIEWPORT, viewport);
//	GLdouble modelview[16] = { 1, 0, 0, 0,  0, 1, 0, 0,  0, 0, 1, 0,  0, 0, 0, 1 };
//	//glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
//	GLdouble projection[16];
//	glGetDoublev(GL_PROJECTION_MATRIX, projection);
//	double winx, winy, winz;
//
//	if (Window::height == 0 || Window::width == 0) {
//		return;
//	}
//
//	// check x
//	gluProject(x-radius, y, z, modelview, projection, viewport, &winx, &winy, &winz);
//	if (winx < 0) {
//		if (!offLeft) {
//			cout << "off screen left!: " << winx << " " << winy << " " << winz << endl;
//			bounce(Vector3(1, 0, 0));
//			offLeft = true;
//			return;
//		}
//	}
//	else {
//		offLeft = false;
//	}
//
//	gluProject(x + radius, y, z, modelview, projection, viewport, &winx, &winy, &winz);
//	if (winx > Window::width) {
//		if (!offRight) {
//			cout << "off screen right!: " << winx << " " << winy << " " << winz << endl;
//			bounce(Vector3(-1, 0, 0));
//			offRight = true;
//			return;
//		}
//	}
//	else {
//		offRight = false;
//	}
//
//	// check y
//	gluProject(x, y - radius, z, modelview, projection, viewport, &winx, &winy, &winz);
//	if (winy < 0) {
//		if (!offBottom) {
//			cout << "off screen bottom!: " << winx << " " << winy << " " << winz << endl;
//			bounce(Vector3(0, 1, 0));
//			offBottom = true;
//			return;
//		}
//	}
//	else {
//		offBottom = false;
//	}
//
//	gluProject(x, y + radius, z, modelview, projection, viewport, &winx, &winy, &winz);
//	if (winy > Window::height) {
//		if (!offTop) {
//			cout << "off screen top!: " << winx << " " << winy << " " << winz << endl;
//			bounce(Vector3(0, -1, 0));
//			offTop = true;
//			return;
//		}
//	}
//	else {
//		offTop = false;
//	}
//
//	// check z
//	gluProject(x, y, z - radius, modelview, projection, viewport, &winx, &winy, &winz);
//	
//	if (winz > 1) {
//		if (z > maxFloor) {
//			maxFloor = z;
//		}
//
//		if (!offFloor) {
//			cout << "off screen floor!: " << winx << " " << winy << " " << winz << endl;
//			bounce(Vector3(0, 0, 1));
//			offFloor = true;
//		}
//
//		if (z < maxFloor) {
//			double diff = maxFloor - z;
//			translate(Vector3(0, 0, diff));
//		}
//	}
//	else {
//		offFloor = false;
//	}
//}
//
//void Cube::bounce(Vector3 n) {
//	Vector3 u = n;
//	u.scale(Vector3::dot(velocity, n));
//	Vector3 w = velocity - u;
//	velocity = w - u;
//	velocity.scale(decay);
//}
//
//void Cube::resetBall() {
//	srand(time(NULL));
//	velocity = Vector3(rand() % 6, rand() % 6, 0);
//	gravity = Vector3(0, 0, -0.005);
//	maxFloor = -numeric_limits<double>::infinity();
//	offLeft = false;
//	offRight = false;
//	offTop = false;
//	offBottom = false;
//	offFloor = false;
//}