#ifndef _BEZEIRCURVE_H_
#define _BEZEIRCURVE_H_

#include <iostream>
#include <vector>
#include "main.h"
#include <GL\glut.h>

#include "Vector4.h"
#include "Matrix4.h"

using namespace std;
// Transistion classes to be used with the scene graph
class Bez{
private:
	//static const Matrix4 B_BEZ(new double[4][4]{{ -1, 3, -3, 1 },
	//{ 3, -6, 3, 0 },
	//{ -3, 3, 0, 0 },
	//{ 1, 0, 0, 0 }});
	Matrix4 points;
	Matrix4 C;
	Matrix4 devPoints;

	vector<Vector4> pointsVect;
	int granularity = 0;
	bool hasStateChanged = true;
public:
	Bez();
	Bez(Matrix4 g_mat);
	Bez(Vector4 & v0, Vector4 & v1, Vector4 & v2, Vector4 & v3);
	//--- constructor for generating a curve
	Bez(Vector4 & v0, Vector4 & v1,
		Vector4 & v2, Vector4 & v3, int granularity);

	// Generates the Bez constant C for the calculations for the times T later
	Bez(Vector4 vects[4]);
	~Bez();

	Vector4 curveAt(double t);
	Vector4 curveAt(int t);
	Vector4 curveAtDev(double t);

	void updatePoint(int controlPoint, int component, double value);
	void updateCtrlPnt(int controlPoint, double x, double y, double z);
	void updateCtrlPnt(int controlPoint, Vector3 & point);

	void buildQList(); // inappropriately named method

	//--- generates the vector for the control point, does NOT modify the internal points matrix
	Vector4 getControlPoint(int position);

	Matrix4 getPointsMatrix() const;

	//--- generate a curve with a granularity
	void generateCurve();

	void draw(Matrix4 C);
	//--- UTILITY FUNCTIONS
	//--- use the following for a more generalized Bernstein curve
	void print(const char * msg);

	double bernsteinCoeff(long n, long i, double t);
	double derBernCoeff(int i, double t);

	long combo(long n, long i);
	long fact(long n);

	const int getGranularity(){
		return granularity;
	}
};

// Considering moving to the scene graph
// BezPatch class contains 16 points and makes use of the algorithm for finding the T's
class BezPatch{
private:
	Bez * c_set[8];
	vector<Vector4> sets;
	vector<Vector4> noms;
	int granularity;
public:
	// constructor for the BezPatch
	BezPatch();
	BezPatch(Bez c_set[8], int granularity);
	BezPatch(Bez b0, Bez b1, Bez b2, Bez b3, Bez b4, Bez b5, Bez b6, Bez b7, int granularity);

	~BezPatch();	// destructor

	void initialize(Bez c_set[8]);
	void update(Bez b0, Bez b1, Bez b2, Bez b3, Bez b4, Bez b5, Bez b6, Bez b7);
	void generatePatch();
	void updateControlPoint(int point, int component, double value);

	//--- tesselate the quads and update
	void draw();
};

class BezCombinator{
private:
	vector<Bez> combinator;
	bool isALoop = false;
	int totalGranularity = 0;
	const int LST = 3;
	const int FST = 0;
public:
	BezCombinator(){}

	bool addBez(Bez b){
		if (isALoop){
			return false;
		}
		if (combinator.size() == 0){
			combinator.push_back(b);
			totalGranularity = b.getGranularity();
		}
		else{
			//--- get the last Bez's last control point
			Vector4 _lst = combinator[combinator.size() - 1].getControlPoint(LST);
			//--- get the new Bez's first control point
			Vector4 _new = b.getControlPoint(FST);
			if (_lst == _new){
				combinator.push_back(b);
				totalGranularity += b.getGranularity();
				cout << "A connection has been made." << endl;
			}
			else{
				cerr << "ERROR: cannot combine the bezier curves together." << endl;
				_lst.print("Last point");
				_new.print("First point");
			}
		}
	}// end routine addBez

	bool addBez(Bez b, int c_n){
		if (c_n == 0 || combinator.size() == 0){
			return addBez(b);
		}
		if (c_n == 1){
			Bez * _b = &combinator[combinator.size() - 1];
			Vector4 _vA = _b->getControlPoint(LST - 1);
			Vector4 _vB = b.getControlPoint(FST + 1);

			Vector3 _newPt = ((_vB + _vA).toVector3()/2);
			_newPt.print("The new point...");
			_b->getControlPoint(LST).print("BEFORE: ");
			_b->updateCtrlPnt(LST, _newPt);
			_b->getControlPoint(LST).print("AFTER: ");
			b.getControlPoint(FST).print("BEFORE");
			b.updateCtrlPnt(FST, _newPt);
			b.getControlPoint(FST).print("AFTER");

			return addBez(b);
		}
		return false;
	}

	Vector4 traverseBy(int delta){
		_ASSERT(combinator.size() > 0);
		int _actualDelta = delta;
		Bez _currentBez = combinator[0];
		for (int i = 0; _actualDelta > 0; i++){
			_currentBez = combinator[i];
			_actualDelta -= _currentBez.getGranularity();
		}
		//--- restore the correct Bez location
		_actualDelta += (delta == 0)?0:_currentBez.getGranularity();

		return _currentBez.curveAt(_actualDelta);
	}

	//--- this routine will close a bez loop up
	void draw(Matrix4 C){
		for (int i = 0; i < combinator.size(); i++){
			combinator[i].draw(C);
		}
	}

	const int getGranularity(){
		return totalGranularity;
	}
};
#endif