#ifndef _VECTOR3_H_
#define _VECTOR3_H_

#include <string>
#include "Vector4.h"

#ifndef _CONSTANTS_
#define X 0
#define Y 1
#define Z 2
#endif

using namespace std;
class Vector4;
class Vector3
{

protected:
	float v[3];   // matrix elements; first index is for rows, second for columns (row-major)

public:
	// constructors
	Vector3();
	Vector3(float, float, float);
	~Vector3();

	// arithmetic operations
	static float dot(const Vector3&, const Vector3&);
	float dot(const Vector3&);
	static Vector3 cross(const Vector3&, const Vector3&);
	Vector3 cross(const Vector3&);
	float length();

	// operator overloads
	Vector3 operator+(const Vector3&);
	Vector3 operator-(const Vector3&);
	Vector3 operator=(const Vector3&);

	bool operator==(const Vector3 & other);
	
	// alias for divisible scaling
	Vector3 operator/(float s);
	// this is a component-wise multiplication
	Vector3 operator*(const Vector3& other);
	float& operator[](int i);

	// in place update operations
	void negate();
	Vector3 scale(float);
	void normalize();

	// accessors
	float x() const;
	float y() const;
	float z() const;

	void print(string) const;
	string toString() const;

	float * getPointer();
	Vector4 toVector4(int type);
};

#endif