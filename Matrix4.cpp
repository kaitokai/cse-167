#include <math.h>
#include <iostream>

#include "Matrix4.h"

Matrix4::Matrix4()
{
	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			m[i][j] = 0;
		}
	}
}

Matrix4::Matrix4(double m[4][4]){
	for (int i = 0; i < 4; ++i){
		for (int j = 0; j < 4; ++j){
			this->m[i][j] = m[i][j];
		}
	}
}

Matrix4::Matrix4(
	double m00, double m01, double m02, double m03,
	double m10, double m11, double m12, double m13,
	double m20, double m21, double m22, double m23,
	double m30, double m31, double m32, double m33)
{
	m[0][0] = m00;
	m[0][1] = m01;
	m[0][2] = m02;
	m[0][3] = m03;

	m[1][0] = m10;
	m[1][1] = m11;
	m[1][2] = m12;
	m[1][3] = m13;

	m[2][0] = m20;
	m[2][1] = m21;
	m[2][2] = m22;
	m[2][3] = m23;

	m[3][0] = m30;
	m[3][1] = m31;
	m[3][2] = m32;
	m[3][3] = m33;
}

Matrix4& Matrix4::operator=(const Matrix4& m2)
{
	if (this != &m2)
	{
		for (int i = 0; i < 4; ++i)
		{
			for (int j = 0; j < 4; ++j)
			{
				m[i][j] = m2.m[i][j];
			}
		}
	}
	return *this;
}

Matrix4& Matrix4::operator*(const Matrix4& m2)
{
	Matrix4 temp;
	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			for (int k = 0; k < 4; ++k)
			{
				temp.m[i][j] += m[i][k] * m2.m[k][j];
			}
		}
	}
	*this = temp;
	return *this;
}

Vector4 Matrix4::operator*(const Vector4& v)
{
	double result[4];
	for (int i = 0; i < 4; ++i)
	{
		result[i] = m[i][0] * v.x() + m[i][1] * v.y() + m[i][2] * v.z() + m[i][3] * v.w();
	}

	return Vector4(result[0], result[1], result[2], result[3]);
}

// return pointer to matrix elements
double* Matrix4::getPointer()
{
	return &m[0][0];
}

// set matrix to identity matrix
void Matrix4::identity()
{
	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			if (i == j)
				m[i][j] = 1.0;
			else
				m[i][j] = 0.0;
		}
	}
}

/* static */ Matrix4 Matrix4::I(){
	Matrix4 _temp;
	for (int i = 0; i < 4; i++){
		_temp.getPointer()[i * 5] = 1;
	}
	return _temp;
}// end routine

// transpose the matrix (mirror at diagonal)
void Matrix4::transpose()
{
	Matrix4 temp;
	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			temp.m[j][i] = m[i][j];
		}
	}
	*this = temp;  // copy temporary values to this matrix
}

// Creates a rotation matrix which rotates about the x axis.
// angle is expected in degrees
void Matrix4::makeRotateX(double angle)
{
	angle = angle / 180.0 * M_PI;  // convert from degrees to radians
	identity();
	m[1][1] = cos(angle);
	m[1][2] = -sin(angle);
	m[2][1] = sin(angle);
	m[2][2] = cos(angle);
}

// Creates a rotation matrix which rotates about the y axis.
// angle is expected in degrees
void Matrix4::makeRotateY(double angle)
{
	angle = angle / 180.0 * M_PI;  // convert from degrees to radians
	identity();
	m[0][0] = cos(angle);
	m[0][2] = sin(angle);
	m[2][0] = -sin(angle);
	m[2][2] = cos(angle);
}

// Creates a rotation matrix which rotates about the z axis.
// angle is expected in degrees
void Matrix4::makeRotateZ(double angle)
{
	angle = angle / 180.0 * M_PI;  // convert from degrees to radians
	identity();
	m[0][0] = cos(angle);
	m[0][1] = -sin(angle);
	m[1][0] = sin(angle);
	m[1][1] = cos(angle);
}

void Matrix4::makeRotate(double angle, const Vector3& axis)
{
	angle = angle / 180.0 * M_PI; // convert from degrees to radians

	double x = axis.x();
	double y = axis.y();
	double z = axis.z();

	m[0][0] = 1 + (1 - cos(angle))*(x*x - 1);
	m[0][1] = -z*sin(angle) + (1 - cos(angle))*x*y;
	m[0][2] = y*sin(angle) + (1 - cos(angle))*x*z;

	m[1][0] = z*sin(angle) + (1 - cos(angle))*y*x;
	m[1][1] = 1 + (1 - cos(angle))*(y*y - 1);
	m[1][2] = -x*sin(angle) + (1 - cos(angle))*y*z;

	m[2][0] = -y*sin(angle) + (1 - cos(angle))*z*x;
	m[2][1] = x*sin(angle) + (1 - cos(angle))*z*y;
	m[2][2] = 1 + (1 - cos(angle))*(z*z - 1);
}

void Matrix4::makeScale(double sx, double sy, double sz)
{
	identity();
	m[0][0] = sx;
	m[1][1] = sy;
	m[2][2] = sz;
}

void Matrix4::makeTranslate(double tx, double ty, double tz)
{
	identity();
	m[0][3] = tx;
	m[1][3] = ty;
	m[2][3] = tz;
}

void Matrix4::makeRotateX_f(double angle){
	Matrix4 temp;
	temp.makeRotateX(angle);

	(*this) = temp * (*this);
}

void Matrix4::makeRotateX_b(double angle){
	Matrix4 temp;
	temp.makeRotateX(angle);

	(*this) * temp;
}

void Matrix4::makeRotateY_f(double angle){
	Matrix4 temp;
	temp.makeRotateY(angle);

	(*this) = temp * (*this);
}

void Matrix4::makeRotateY_b(double angle){
	Matrix4 temp;
	temp.makeRotateY(angle);

	(*this) * temp;
}

void Matrix4::makeScale_f(double x, double y, double z){
	Matrix4 temp;
	temp.makeScale(x, y, z);

	(*this) = temp * (*this);
}

void Matrix4::makeScale_b(double x, double y, double z){
	Matrix4 temp;
	temp.makeScale(x, y, z);

	(*this) * temp;
}

void Matrix4::makeRotateZ_f(double angle){
	Matrix4 temp;
	temp.makeRotateZ(angle);

	(*this) = temp * (*this);
}

void Matrix4::makeRotateZ_b(double angle){
	Matrix4 temp;
	temp.makeRotateZ(angle);

	(*this) * temp;
}
// Creates an internal translation matrix and applies it P' = T * P
void Matrix4::makeTranslate_f(double tx, double ty, double tz){
	Matrix4 temp;
	temp.makeTranslate(tx, ty, tz);
	*this = temp * (*this);
}// end routine

// Creates an internal translation matrix and applies it P' = P * T
void Matrix4::makeTranslate_b(double tx, double ty, double tz){
	Matrix4 temp;
	temp.makeTranslate(tx, ty, tz);
	(*this) * temp;
}// end routine 

//--- inserts the matrix where x is the row and y is the column
void Matrix4::insert(int x, int y, double val){
	m[x][y] = val;
}

void Matrix4::print(string comment)
{
	cout << comment << endl;
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			cout << m[i][j] << "\t";
		}
		cout << endl;
	}
}

Vector3 Matrix4::getPosition() const
{
	return Vector3(m[0][3], m[1][3], m[2][3]);
}

void Matrix4::getRotation(const Matrix4& m2){
	this->identity();
	for (int i = 0; i < 3; i++){
		for (int j = 0; j < 3; j++){
			m[i][j] = m2.m[i][j];
		}
	}
}// end routine getRotation

void Matrix4::getTranslation(const Matrix4& m2){
	this->identity();
	for (int i = 0; i < 3; i++){
		m[i][3] = m2.m[i][3];
	}
}// end routine getTranslation

void Matrix4::makeScale(const Vector3 & vector){
	makeScale(vector.x(), vector.y(), vector.z());
}
void Matrix4::makeScale_f(const Vector3 & vector){
	makeScale_f(vector.x(), vector.y(), vector.z());
}
void Matrix4::makeScale_b(const Vector3 & vector){
	makeScale_b(vector.x(), vector.y(), vector.z());
}
void Matrix4::makeTranslate(const Vector3 & vector){
	makeTranslate(vector.x(), vector.y(), vector.z());
}
void Matrix4::makeTranslate_f(const Vector3 & vector){
	makeTranslate_f(vector.x(), vector.y(), vector.z());
}
void Matrix4::makeTranslate_b(const Vector3 & vector){
	makeTranslate_b(vector.x(), vector.y(), vector.z());
}


//Based on code by cool people on the internet
//http://stackoverflow.com/questions/2624422/efficient-4x4-matrix-inverse-affine-transform
Matrix4 Matrix4::inverse(void)
{
	double s0 = m[0][0] * m[1][1] - m[1][0] * m[0][1];
	double s1 = m[0][0] * m[1][2] - m[1][0] * m[0][2];
	double s2 = m[0][0] * m[1][3] - m[1][0] * m[0][3];
	double s3 = m[0][1] * m[1][2] - m[1][1] * m[0][2];
	double s4 = m[0][1] * m[1][3] - m[1][1] * m[0][3];
	double s5 = m[0][2] * m[1][3] - m[1][2] * m[0][3];

	double c5 = m[2][2] * m[3][3] - m[3][2] * m[2][3];
	double c4 = m[2][1] * m[3][3] - m[3][1] * m[2][3];
	double c3 = m[2][1] * m[3][2] - m[3][1] * m[2][2];
	double c2 = m[2][0] * m[3][3] - m[3][0] * m[2][3];
	double c1 = m[2][0] * m[3][2] - m[3][0] * m[2][2];
	double c0 = m[2][0] * m[3][1] - m[3][0] * m[2][1];

	// Should check for 0 determinant
	double det = (s0 * c5 - s1 * c4 + s2 * c3 + s3 * c2 - s4 * c1 + s5 * c0);
	double invdet = 1.0 / det;

	Matrix4 b;

	b.m[0][0] = (m[1][1] * c5 - m[1][2] * c4 + m[1][3] * c3) * invdet;
	b.m[0][1] = (-m[0][1] * c5 + m[0][2] * c4 - m[0][3] * c3) * invdet;
	b.m[0][2] = (m[3][1] * s5 - m[3][2] * s4 + m[3][3] * s3) * invdet;
	b.m[0][3] = (-m[2][1] * s5 + m[2][2] * s4 - m[2][3] * s3) * invdet;

	b.m[1][0] = (-m[1][0] * c5 + m[1][2] * c2 - m[1][3] * c1) * invdet;
	b.m[1][1] = (m[0][0] * c5 - m[0][2] * c2 + m[0][3] * c1) * invdet;
	b.m[1][2] = (-m[3][0] * s5 + m[3][2] * s2 - m[3][3] * s1) * invdet;
	b.m[1][3] = (m[2][0] * s5 - m[2][2] * s2 + m[2][3] * s1) * invdet;

	b.m[2][0] = (m[1][0] * c4 - m[1][1] * c2 + m[1][3] * c0) * invdet;
	b.m[2][1] = (-m[0][0] * c4 + m[0][1] * c2 - m[0][3] * c0) * invdet;
	b.m[2][2] = (m[3][0] * s4 - m[3][1] * s2 + m[3][3] * s0) * invdet;
	b.m[2][3] = (-m[2][0] * s4 + m[2][1] * s2 - m[2][3] * s0) * invdet;

	b.m[3][0] = (-m[1][0] * c3 + m[1][1] * c1 - m[1][2] * c0) * invdet;
	b.m[3][1] = (m[0][0] * c3 - m[0][1] * c1 + m[0][2] * c0) * invdet;
	b.m[3][2] = (-m[3][0] * s3 + m[3][1] * s1 - m[3][2] * s0) * invdet;
	b.m[3][3] = (m[2][0] * s3 - m[2][1] * s1 + m[2][2] * s0) * invdet;

	return b;
}

Matrix4& Matrix4::mult(const Matrix4& m2)
{
	Matrix4 thisMat = *this; 
	float tmp;
	for (int i = 0; i<4; ++i)
	{
		for (int j = 0; j<4; ++j)
		{
			tmp = 0; 
			for (int k = 0; k<4; ++k)
			{
				tmp += thisMat.m[i][k] * m2.m[k][j];
			}
			m[i][j] = tmp;
		}
	}
	return *this;
}

Vector4 Matrix4::getCol(int index) {
	Vector4 result(0, 0, 0, 0);
	for (int i = 0; i < 4; i++) {
		result[i] = m[i][index];
	}
	return result;
}