#ifndef _MATERIAL_H_
#define _MATERIAL_H_
#include "XYZParser.h"
class Material{
private:
	float * ambient;
	float * specular;
	float * diffuse;
	float * shininess;
	float * emission;
public:
	Material(){ }
	void apply(int i){
		glClearColor(0.0, 0.0, 0.0, 0.0);
		//glMaterialfv(GL_FRONT, GL_AMBIENT, ambient);
		//glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
		//glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse);
		//glMaterialfv(GL_FRONT, GL_SHININESS, shininess);
		//glMaterialfv(GL_FRONT, GL_EMISSION, emission);
		if (i == 0){
			glColorMaterial(GL_FRONT, GL_SHININESS);
			GLfloat mat_amb_diff[] = { 0.1, 0.5, 0.8, 1.0 };
			glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE,
				mat_amb_diff);
			//glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 30);
		}
		if (i == 1){
			glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
			glMaterialf(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, 30);
		}
		if (i == 2){
			glColorMaterial(GL_FRONT_AND_BACK, GL_SPECULAR);
			glMaterialf(GL_FRONT_AND_BACK, GL_SPECULAR, 30);
		}
	}
};
#endif