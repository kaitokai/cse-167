#ifndef _CUBE_H_
#define _CUBE_H_

#include "geode.h"

class Cube : public Geode{
private:

public:
	void draw(Matrix4 C);
	void render();
};

#endif
/*
#include "Matrix4.h"
#include "Vector3.h"
#include <GL/glut.h>

class Cube
{
  protected:
    Matrix4 model2world;            // model matrix (transforms model coordinates to world coordinates)
	Vector3 velocity;
	Vector3 gravity;
	double radius;
	double decay;
	double maxFloor;
	bool offLeft;
	bool offRight;
	bool offTop;
	bool offBottom;
	bool offCeiling;
	bool offFloor;
	void checkCollision();
	void bounce(const Vector3);
	void resetBall();

  public:
    Cube();   // Constructor
    Matrix4& getMatrix();
    void spin(double);      // spin cube [degrees]
	void translate(Vector3);
	void scale(double);
	void orbit(double);
	void printPosition(string message) const;
	void reset();
	void draw(bool drawBall=false) const;
	void updatePosition();
};
#endif
*/