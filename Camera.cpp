#include "Camera.h"

Camera::Camera(){

}// end constructor

Camera::Camera(Vector3 e, Vector3 d, Vector3 up){
	this->e = e;
	this->d = d;
	this->up = up;

	update();
}// end constructor

void Camera::update(){
	Vector3 z_c = (e - d) / ((e - d).length());
	Vector3 x_c = (up.cross(z_c)) / ((up.cross(z_c)).length());
	Vector3 y_c = (z_c.cross(x_c)) / ((z_c.cross(x_c)).length());

	C.identity();
	double * ptr = C.getPointer();
	for (int i = 0; i < 3; i++){
		ptr[i * 4] = x_c.getPointer()[i];
		ptr[i * 4 + 1] = y_c.getPointer()[i];
		ptr[i * 4 + 2] = z_c.getPointer()[i];
		ptr[i * 4 + 3] = e.getPointer()[i];
	}
}
//--- returns GLdouble[16] array
GLdouble * Camera::getGLMatrix(){
	
	//temp.transpose(); //--- convert to Col-Major
	Matrix4 temp = inverse();

	temp.transpose();
	double * arr = temp.getPointer();

	for (int i = 0; i < MAT_SIZE; i++){
		t[i] = (*(arr + i));
	}

	return t;
}// end routine getGLMatrix

Matrix4 Camera::inverse(){
	Matrix4 temp;

	Matrix4 R, T;
	R.getRotation(C);
	R.transpose();

	T.getTranslation(C);
	for (int i = 0; i < 3; i++){
		T.getPointer()[(i * 4) + 3] *= -1;
	}

	temp = R * T;

	return temp;
}

void Camera::set(Vector3 projection, Vector3 look, Vector3 up){
	this->e = projection;
	this->d = look;
	this->up = up;
	update();
}

Vector3 Camera::getEyePos(void)
{
	return e;
}

