#ifndef _main_h_
#define _main_h_
//#include <GL\glew.h>

#include "GLee.h"
#include "Window.h"
//#include "cube.h"
//#include "matrix4.h"
//#include "house.h"
//
//#include "Cube.h"
//#include "House.h"
//#include "Matrix4.h"
//#include "Vector3.h"
#include "matrixTransform.h"
#include "Material.h"
#include "Camera.h"

namespace Globals
{
 //   extern cube cube;
	//extern house house;
	//extern bool spinclockwise;
	//extern bool isball;
	extern Group root;
	extern Camera c2w;
	extern Shader * shader;
	extern Shader * refShader;
	extern Material * mat;
	extern bool isEnvOn;
	extern bool isPointsDrawn;
	
	extern bool isWireFrameCurveOn;
	extern bool areControlPointsDrawn;
	extern bool isWireFramePatchOn;
};

#endif