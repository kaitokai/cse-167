#include "BezierCurve.h"

///////////////////////////////////////////////////////////////////////////////
//BEZ CURVE////////////////////////////////////////////////////////////////////
Bez::Bez(){

}

Bez::Bez(Matrix4 g_mat){
	//points = new Matrix4();
	//devPoints = new Matrix4();


	points = g_mat;
	Matrix4 _temp = g_mat;

	double m[4][4] = {
			{ -1, 3, -3, 1 },
			{ 3, -6, 3, 0 },
			{ -3, 3, 0, 0 },
			{ 1, 0, 0, 0 }
	};
	Matrix4 B_BEZ = Matrix4(m);
	C = (_temp * B_BEZ);

	buildQList();
}

Bez::Bez(Vector4 & v0, Vector4 & v1, Vector4 & v2, Vector4 & v3){
	Vector4 vects[4] = { v0, v1, v2, v3 };
	//vector<vector4> vects(4);
	//vects.push_back(v0); vects.push_back(v1); vects.push_back(v2); vects.push_back(v3);
	//points = new Matrix4();
	//devPoints = new Matrix4();

	//cout << "Creating the matrix.." << endl;
	for (int i = 0; i < 4; i++){
		//cerr << "ON index: " << i << " " << vects[i].w() << endl;
		_ASSERT(vects[i].w() == 1); // needs to be a point
		points.insert(0, i, vects[i].x());
		points.insert(1, i, vects[i].y());
		points.insert(2, i, vects[i].z());
		//			p[i] = *new Vector4(vects[i].x(), vects[i].y(), vects[i].z(), vects[i].w());
	}

	//points->print("G matrix: ");
	Matrix4 _temp = points;

	double m[4][4] = {
			{ -1, 3, -3, 1 },
			{ 3, -6, 3, 0 },
			{ -3, 3, 0, 0 },
			{ 1, 0, 0, 0 }
	};
	Matrix4 B_BEZ = Matrix4(m);
	C = (_temp * B_BEZ); // THIS will create the constants C
	buildQList();
	//--- END OF DER BEZ STUFF
}
Bez::Bez(Vector4 & v0, Vector4 & v1, Vector4 & v2, Vector4 & v3, int granularity):Bez(v0, v1, v2, v3){
	this->granularity = granularity;
}

// Generates the Bez constant C for the calculations for the times T later
Bez::Bez(Vector4 vects[4]) :Bez(vects[0], vects[1], vects[2], vects[3]){
}

Bez::~Bez(){
}

Vector4 Bez::curveAt(double t){
	_ASSERT(0 <= t && t <= 1);
	Vector4 deltas(t*t*t, t*t, t, 1);
	Vector4 q = (C * deltas);
	q.getPointer()[3] = 1;
	return q;
}

Vector4 Bez::curveAt(int i){
	return curveAt(double(i) / double(granularity));
}

Vector4 Bez::curveAtDev(double t){
	_ASSERT(0 <= t && t <= 1);
	Vector4 c(derBernCoeff(0, t), derBernCoeff(1, t), derBernCoeff(2, t), 0);
	Vector4 q = devPoints * c;
	q.getPointer()[3] = 0;
	return q;
}

//--- Update the control point for the Bezier curve where controlPoint in [0,3]
//--- and component is 0 to 2, where X = 0, Y = 1, and Z = 3
void Bez::updatePoint(int controlPoint, int component, double value){
	_ASSERT(0 <= controlPoint && controlPoint < 4); // we only have four control points per Bezier
	int comp = controlPoint + (4 * component); // this will get us our component for the point
	points.getPointer()[comp] = value;

	buildQList(); // making sure to update the new list of q points for the tangents

	double m[4][4] = {
			{ -1, 3, -3, 1 },
			{ 3, -6, 3, 0 },
			{ -3, 3, 0, 0 },
			{ 1, 0, 0, 0 }
	};
	Matrix4 B_BEZ = Matrix4(m);
	Matrix4 _temp = points;
	C = (_temp * B_BEZ); // THIS will create the constants C
	hasStateChanged = true; // let the program know that the state has changed
}

void Bez::updateCtrlPnt(int controlPoint, double x, double y, double z){
	updatePoint(controlPoint, X, x);
	updatePoint(controlPoint, Y, y);
	updatePoint(controlPoint, Z, z);
}

void Bez::updateCtrlPnt(int controlPoint, Vector3 & point){
	updateCtrlPnt(controlPoint, point.x(), point.y(), point.z());
}

void Bez::buildQList(){
	double * v = points.getPointer();
	double * m = devPoints.getPointer();
	for (int i = 0; i < 3; i++){
		m[4 * X + i] = v[4 * X + i + 1] - v[4 * X + i];
		m[4 * Y + i] = v[4 * Y + i + 1] - v[4 * Y + i];
		m[4 * Z + i] = v[4 * Z + i + 1] - v[4 * Z + i];
	}
	//		*devPoints * D_BEZ;
}

Matrix4 Bez::getPointsMatrix() const{
	return points;
}

void Bez::print(const char * msg){
	cout << msg << ": " << endl;
	points.print("");
}

double Bez::bernsteinCoeff(long n, long i, double t){
	return combo(n, i) * pow(1.0 - t, n - i) * pow(t, i);
}

double Bez::derBernCoeff(int i, double t){
	if (i == 0 || i == 2) return pow(t, i) * pow(1 - t, 2 - i) * 3;
	else return 2 * t * (1 - t) * 3;
	//		return combo(2, i) * pow(t, i) * pow(1-t, 2-i) * 3;
}

long Bez::combo(long n, long i){
	return fact(n) / (fact(n - i) * fact(i));
}// end routine combo

long Bez::fact(long n){
	long res = 1;
	for (long i = n; i > 0; i--){
		res *= i;
	}
	return res;
}// end routine fact

void Bez::generateCurve(){
	if (!hasStateChanged){
		//--- do not continue on to render and waste resources
		return;
	}

	pointsVect.clear();
	if (granularity == 0){
		cerr << "WARNING: Granularity component for b_curve has not been set" << endl;
		cerr << "Cannot draw curve... returning" << endl;
		return;
	}

	double ratio = double(1) / double(granularity);
	//--- get each and all points to approximate a line
	for (int i = 0; i < granularity; i++){
		Vector4 ptr = curveAt(ratio * i);
		pointsVect.push_back(ptr);
	}

	_ASSERT(granularity == pointsVect.size());

	hasStateChanged = false;
}// end routine generateCurve

//--- draw the curve to screen
void Bez::draw(Matrix4 C){
	glMatrixMode(GL_MODELVIEW);
	C.transpose();
	glLoadMatrixd(C.getPointer());

	//--- check to see if the curve's state has been changed and render it
	generateCurve();

	//--- if the control points are to be drawn, we can do so here

	double factor = 0.05;
	if (Globals::areControlPointsDrawn){
		vector<Vector4> _ctrlPts;

		glBegin(GL_QUADS); {
			glColor3d(0.5, 0.5, 0);
			for (int i = 0; i < 4; i++){
				Vector4 _cPoint = getControlPoint(i);

				glVertex3d(_cPoint.x() - factor, _cPoint.y() - factor, _cPoint.z());
				glVertex3d(_cPoint.x() + factor, _cPoint.y() - factor, _cPoint.z());
				glVertex3d(_cPoint.x() + factor, _cPoint.y() + factor, _cPoint.z());
				glVertex3d(_cPoint.x() - factor, _cPoint.y() + factor, _cPoint.z());

				_ctrlPts.push_back(_cPoint);
			}
		} glEnd();

		glBegin(GL_LINES);
			glColor3d(0, 0.5, 0.1);
			for (int i = 0; i < 3; i++){
				Vector4 _pA = _ctrlPts[i];
				Vector4 _pB = _ctrlPts[i + 1];

				glVertex3d(_pA.x(), _pA.y(), _pA.z());
				glVertex3d(_pB.x(), _pB.y(), _pB.z());
			}
		 glEnd();
	}

	//--- use a white line for the lines
	glColor3d(1.0, 1.0, 1.0);
	glBegin(GL_LINES);
		for (int i = 0; i < granularity - 1; i++){
			Vector4 _pointA = pointsVect[i];
			Vector4 _pointB = pointsVect[i + 1];
			glVertex3d(_pointA.x(), _pointA.y(), _pointA.z());
			glVertex3d(_pointB.x(), _pointB.y(), _pointB.z());
		}
	glEnd();
}//end routine draw


Vector4 Bez::getControlPoint(int position){
	//--- ensure that the programmer/program only looks for points between 0-3
	_ASSERT(0 <= position && position <= 3);

	double * _points = points.getPointer();
	int _x = _points[position];
	int _y = _points[4 + position];
	int _z = _points[8 + position];

	return Vector4(_x, _y, _z, 1);
}// end routine getControlPoint

///////////////////////////////////////////////////////////////////////////////
//BEZ PATCH ///////////////////////////////////////////////////////////////////

BezPatch::BezPatch(){

}

BezPatch::BezPatch(Bez c_set[8], int granularity){
	sets.reserve(granularity * granularity);
	this->granularity = granularity;
	cout << "initializing B_Patch" << endl;
	initialize(c_set);
}// end cons

BezPatch::BezPatch(Bez b0, Bez b1, Bez b2, Bez b3, Bez b4, Bez b5, Bez b6, Bez b7, int granularity){
	Bez c_set[8] = { b0, b1, b2, b3, b4, b5, b6, b7 };
	sets.reserve(granularity * granularity);
	this->granularity = granularity;
	cout << "initializing B_Patch" << endl;
	initialize(c_set);
}

BezPatch::~BezPatch(){
	sets.clear();
}

void BezPatch::initialize(Bez c_set[8]){
	for (int i = 0; i < 8; i++){
		this->c_set[i] = new Bez(c_set[i].getPointsMatrix());
	}
	generatePatch();
}

void BezPatch::update(Bez b0, Bez b1, Bez b2, Bez b3, Bez b4, Bez b5, Bez b6, Bez b7){
	Bez c_set[8] = { b0, b1, b2, b3, b4, b5, b6, b7 };
	initialize(c_set);
}

void BezPatch::generatePatch(){
	sets.clear(); noms.clear(); // make sure to clear out the entire vectors to save space
	double ratio = double(1) / granularity;

	int U_DIR = 4;
	int V_DIR = 8;

	//		cout << ratio << endl;
	Vector4 q_u[4];
	Vector4 q_v[4];

	//--- THIS GENERATES THE CURVE FROM U->V, where LERP(V(LERP(U),LERP(U))
	for (int u = 0; u < granularity; u++){
		//--- generate the q_points for the time where u = i
		for (int j = 0; j < U_DIR; j++){
			//--- CREATE THE CURRENT U-BEZIER CURVE
			q_u[j] = c_set[j]->curveAt(ratio * u);     // r = (u
		}

		Bez q_bez(q_u); // let's make a bezier curve at the points made in q

		//--- build up the points for the time where v = k s.t. x(u,v) = Bez(v, q[])
		//--- this is for tracking the curves along the u-q line
		for (int v = 0; v < granularity; v++){
			Vector3 tan_u = q_bez.curveAtDev(ratio * v).toVector3();
			tan_u.normalize(); // normalize the inner component of the tangent vector

			for (int j = 0; j + U_DIR < V_DIR; j++){
				q_v[j] = c_set[j + U_DIR]->curveAt(ratio * v); // get our s points
			}
			Bez q_bezv(q_v); // 
			Vector3 tan_v = q_bezv.curveAtDev(ratio * u).toVector3(); // (u, s0, s1, s2, s3)
			tan_v.normalize();

			Vector4 normal = tan_v.cross(tan_u).toVector4(0);
			noms.push_back(normal);

			Vector4 u_0 = q_bez.curveAt(ratio * v); // x(u,v)
			sets.push_back(u_0);
		}
	}// O(n*n)
	//--- END

	//cerr << "SIZE: " << sets.size() << endl;
}// end routine generatePatch

void BezPatch::updateControlPoint(int point, int component, double value){
	_ASSERT(0 <= point && point < 16 && 0 <= component && component < 3);
	// eg. we want to get control point 16
	// 16/4 = 4, 4 - 1 = [3]
	// for the point within that, it should be 4
	// 16 % 4 = 0 - 1 = 3, which is point 16
	// we want to mess with the component (where the component is 0 to 2) 2 (ie. Z)
	//--- to get this component we simply just do this math: position + (4 * component)
	//--- for this example we get 3 + 4 * 2 = 3 + 8 = 11, which is the z comp

	//--- MATHEMATICS for the U-Beziers
	int arrLoc = (point / 4); // divide the point location by four to get array location in c_set
	int inCntrlPnt = (point) % 4; // obtain the actual internal component with in the c_set
	//		int comp = inCntrlPnt + 4 * component; // get the component (x,y,z)
	c_set[arrLoc]->updatePoint(inCntrlPnt, component, value);
	c_set[inCntrlPnt + 4]->updatePoint(arrLoc, component, value); // simply swap the two params to grab the other points
}// end routine updateControlPoint

void BezPatch::draw(){
	generatePatch();
	glMatrixMode(GL_MODELVIEW);

	//--- code for alternative drawing
	if (Globals::isWireFramePatchOn){
		glBegin(GL_LINES);
	}
	else{
		glBegin(GL_QUADS);
	}
	//--- some code for the debug
	//--- 
	glColor3d(1, 0, 0);
	int n = granularity - 1;
	int m = granularity;
	for (int i = 0; i < n; i++){
		int rowLoc = i * n;
		for (int j = 0; j < n; j++){
			int pos = rowLoc + j; // initial position
			int posP1 = rowLoc + j + 1;
			//--- draw the quads in the following pattern
			//--- assuming that we begin to draw from the bottom-left corner to the top right corner
			// x
			int p3 = m*j + i;
			int p2 = m*(j + 1) + i;
			int p1 = m*(j + 1) + (i + 1);
			int p0 = m*j + i + 1;

			glNormal3d(noms[p0].x(), noms[p0].y(), noms[p0].z());
			glVertex3d(sets[p0].x(), sets[p0].y(), sets[p0].z());

			glNormal3d(noms[p1].x(), noms[p1].y(), noms[p1].z());
			glVertex3d(sets[p1].x(), sets[p1].y(), sets[p1].z());

			if (Globals::isWireFramePatchOn){
				glNormal3d(noms[p1].x(), noms[p1].y(), noms[p1].z());
				glVertex3d(sets[p1].x(), sets[p1].y(), sets[p1].z());
			}

			glNormal3d(noms[p2].x(), noms[p2].y(), noms[p2].z());
			glVertex3d(sets[p2].x(), sets[p2].y(), sets[p2].z());

			if (Globals::isWireFramePatchOn){
				glNormal3d(noms[p2].x(), noms[p2].y(), noms[p2].z());
				glVertex3d(sets[p2].x(), sets[p2].y(), sets[p2].z());
			}

			glNormal3d(noms[p3].x(), noms[p3].y(), noms[p3].z());
			glVertex3d(sets[p3].x(), sets[p3].y(), sets[p3].z());

			if (Globals::isWireFramePatchOn){
				glNormal3d(noms[p3].x(), noms[p3].y(), noms[p3].z());
				glVertex3d(sets[p3].x(), sets[p3].y(), sets[p3].z());
			}

			if (Globals::isWireFramePatchOn){
				glNormal3d(noms[p0].x(), noms[p0].y(), noms[p0].z());
				glVertex3d(sets[p0].x(), sets[p0].y(), sets[p0].z());
			}
		}
	}
	glEnd();

	if (Globals::isPointsDrawn){
		glBegin(GL_POINTS);
		for (unsigned int i = 0; i < sets.size(); i++){
			glColor3d(0, 1, 0);

			glVertex3d(sets[i].x(), sets[i].y(), sets[i].z());
		}
		glEnd();
	}
}// end routine draw
