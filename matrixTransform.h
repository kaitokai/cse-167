#ifndef _MATRIX_TRANSFORM_H_
#define _MATRIX_TRANSFORM_H_

#include "group.h"

class MatrixTransform : public Group{
private:
	Matrix4 M;
public:
	//MatrixTransform(char * name, Matrix4 m2w, Matrix4 M);
	MatrixTransform(Matrix4 M);
	void draw(Matrix4 C);
	void update();
	Matrix4& getM(){
		return M;
	}
};

#endif
