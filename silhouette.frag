//////////////////////////////////
// FRAGMENT SHADER ///////////////
#version 120

varying float edge;
varying vec3 normal;

uniform vec3 colors;

void main(){

	float intensity;
	vec4 color;
	vec3 n = normalize(normal);
	
	intensity = dot(vec3(gl_LightSource[1].position), n);

/*	if(edge > .1)
		color = vec4(0, 0, 0, 1);
	else if (intensity > 0.95)
		color = vec4(colors[0], colors[1], colors[2], 1.0);
	else if (intensity > 0.65)
		color = vec4(0.75 * colors[0], 0.75 * colors[1], 0.75 * colors[2], 1.0);
	else if (intensity > 0.25)
		color = vec4(0.5 * colors[0], 0.5 * colors[1], 0.5 * colors[2], 1.0);
	else
		color = vec4(0.3 * colors[0], 0.3 * colors[1], 0.3 * colors[2], 1.0);
*/
color = vec4(sqrt(intensity) * 4);
	gl_FragColor = color;
}

//--- EOD ---//