#ifndef _SPHERE_H_
#define _SPHERE_H_

#include "geode.h"

class Sphere : public Geode{
private:
	double radius;
public:
	//--- constructors
	// constructs a sphere with a unit radius
	Sphere();
	// constructs a sphere with defined radius
	Sphere(double radius);

	//--- routines
	void draw(Matrix4 C);
	void update();

	void render();
};

#endif