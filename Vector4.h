#ifndef _VECTOR4_H_
#define _VECTOR4_H_

#include <string>
#include "Vector3.h"

using namespace std;
class Vector3;
class Vector4
{

protected:
	float v[4];   // matrix elements; first index is for rows, second for columns (row-major)

public:
	// constructors
	Vector4();
	Vector4(float, float, float, float);
	~Vector4();

	// operator overloads
	Vector4 operator+(const Vector4&);
	Vector4 operator-(const Vector4&);
	bool operator==(const Vector4 & other);

	Vector3 toVector3();
	void internalNormalize();
	Vector4 internalCross(const Vector4 &);
	// in place update operations
	void dehomogenize();

	void print(string) const;
	string toString() const;
	float * getPointer();
	float length();
	//******
	void scale(const float& dval);
	inline float& operator[](int in) {
		return v[in];
	}

	// accessors
	float x() const;
	float y() const;
	float z() const;
	float w() const;
};

#endif