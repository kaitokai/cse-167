#include "sphere.h"

Sphere::Sphere() : Sphere(1){}

Sphere::Sphere(double radius):radius(radius){
}

void Sphere::draw(Matrix4 C){
//	applyM2W(C);
	Geode::draw(C);
	render();
}// end routine draw

void Sphere::update(){

}//--- update the bounding sphere

void Sphere::render(){
	glutSolidSphere(radius, 20, 20);
}// end routine render

// EOD