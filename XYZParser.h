#ifndef _XYZPARSER_H_
#define _XYZPARSER_H_
#include <iostream>
#include <vector>
#include <string>
//#include <GL\glew.h>

#include <GL\glut.h>

#include "Vector3.h"
#include "Matrix4.h"
#include "Triangle.h"

#ifndef _CONSTANTS_
#define X 0
#define Y 1
#define Z 2
#endif

#include "geode.h"

using namespace std;

class XYZParser : public Geode{
private:
	vector<Vector4> verts;
	vector<Vector3> norms;
	vector<Vector3> colrs;
	
	vector<Triangle> vert_x;
//	vector<int> index;
	Matrix4 m2w;
	Matrix4 m2w_init;

	bool isInitialized;

//	double minmax_xyz[6];
	double min[3];
	double max[3];

	double cntr[3];
	double scFact;

	void setMinMaxArr();
	void checkForMinMax(double x, double y, double z);
	void centerAndScale(){
		Matrix4 T, S;
		T.makeTranslate(-cntr[X], -cntr[Y], -cntr[Z]); // why does negation work here?
		T.print("Translation Matrix to Center: ");
		printf("Scale factor is: %f\n", scFact);
		S.makeScale(scFact, scFact, scFact);
		S.print("Scaling Matrix to fit screen: ");
		m2w.identity();
		m2w.print("Old m2w");
		m2w = S * T * m2w;
		m2w.print("New m2w");
		m2w_init = m2w; // keep the original matrix position for the center
		cout << "Finished printing and scaling" << endl;
	}

public:
	//--- constructors
	XYZParser();
	XYZParser(const char * fname, const char type){
		parse(fname, type);
	}
	//	XYZParser(const char * fname);
	//XYZParser(double * verts, double * colrs);

	~XYZParser();

	//--- routines
	void parse(const char * fname);
	void parse(const char * fname, const char type);
	void parse(double verticies[], double colors[], int size);

	//--- arithmetic
	//XYZParser operator=(const XYZParser& other);

	//--- accessors
	const Vector4 getVerts(int i);

	Vector3 getNorms(int i);
	Matrix4& getMatrix();

	void reset();
	int count();
	bool isInit() const;

	void draw(Matrix4 C);
	void render();
	void update();

	void print(){
		printf("%d\n", verts.size());
	}
};
#endif
