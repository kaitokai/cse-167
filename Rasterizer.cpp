//#ifdef _WIN32
//#include <windows.h>
//#endif
//
//#include <iostream>
//#include <math.h>
//#include <GL/gl.h>
//#include <GL/glut.h>
//
//#include "XYZParser.h"
//#include "Camera.h"
//
//Vector3 shaderizing(const Vector3&, const Vector3&, Vector3, double);
//
//static int window_width = 512, window_height = 512;
//static float* pixels = new float[window_width * window_height * 3];
//
////---
////---
//
//using namespace std;
//
//void displayCallback();
//void setPixel(Vector3 point, Vector3 color);
//
//struct Color    // generic color class
//{
//	float r, g, b;  // red, green, blue
//};
//
//XYZParser rabbit;
//XYZParser dragon;
//XYZParser house;
//XYZParser * M;
//vector<vector<double>> z_buffer;
//void loadData()
//{
//	//
//	rabbit.parse("C:/Users/User/Source/Repos/CSE167repo/PointModels/bunny.xyz");
//	dragon.parse("C:/Users/User/Source/Repos/CSE167repo/PointModels/dragon.xyz");
//
//	double vertices[] = {
//		-4, -4, 4, 4, -4, 4, 4, 4, 4, -4, 4, 4,     // front face
//		-4, -4, -4, -4, -4, 4, -4, 4, 4, -4, 4, -4, // left face
//		4, -4, -4, -4, -4, -4, -4, 4, -4, 4, 4, -4,  // back face
//		4, -4, 4, 4, -4, -4, 4, 4, -4, 4, 4, 4,     // right face
//		4, 4, 4, 4, 4, -4, -4, 4, -4, -4, 4, 4,     // top face
//		-4, -4, 4, -4, -4, -4, 4, -4, -4, 4, -4, 4, // bottom face
//
//		-20, -4, 20, 20, -4, 20, 20, -4, -20, -20, -4, -20, // grass
//		-4, 4, 4, 4, 4, 4, 0, 8, 4,                       // front attic wall
//		4, 4, 4, 4, 4, -4, 0, 8, -4, 0, 8, 4,               // left slope
//		-4, 4, 4, 0, 8, 4, 0, 8, -4, -4, 4, -4,             // right slope
//		4, 4, -4, -4, 4, -4, 0, 8, -4 };                   // rear attic wall
//
//	double colors[] = {
//		1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0,  // front is red
//		0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0,  // left is green
//		1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0,  // back is red
//		0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0,  // right is green
//		0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1,  // top is blue
//		0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1,  // bottom is blue
//
//		0, 0.5, 0, 0, 0.5, 0, 0, 0.5, 0, 0, 0.5, 0, // grass is dark green
//		0, 0, 1, 0, 0, 1, 0, 0, 1,                // front attic wall is blue
//		1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0,         // left slope is green
//		0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0,         // right slope is red
//		0, 0, 1, 0, 0, 1, 0, 0, 1 };              // rear attic wall is red
//
//	house.parse(vertices, colors, sizeof(vertices) / sizeof(double));
//	M = &dragon;
//}
//
//void z_buffer_init(int w, int h){
//	z_buffer.clear();
//	z_buffer.reserve(w);
//	for (int y = 0; y < w; y++){
//		z_buffer.push_back(*(new vector<double>(h, 1)));
//	}
//}
//
//
//// Clear frame buffer
//void clearBuffer()
//{
//	Color clearColor = { 0.0, 0.0, 0.0 };   // clear color: black
//	for (int i = 0; i < window_width*window_height; ++i)
//	{
//		pixels[i * 3] = clearColor.r;
//		pixels[i * 3 + 1] = clearColor.g;
//		pixels[i * 3 + 2] = clearColor.b;
//	}
//	z_buffer_init(window_width, window_height);
//}
//
//// Draw a point into the frame buffer
//bool isSizedPts = false;
//void drawPixels(int x, int y, float r, float g, float b){
//	if (!(x < window_width && x >= 0 && y >= 0 && y < window_height)) {
//		return;
//	}
//	int offset = y * window_width * 3 + x * 3;
//	pixels[offset] = r;
//	pixels[offset + 1] = g;
//	pixels[offset + 2] = b;
//}
//void setPixel(int x, int y, double z){
//	_ASSERT(0 <= z && z <= 1);
//	if (y >= window_height || x >= window_width) return;
//
//	if (z < z_buffer[y][x]){
//		z_buffer[y][x] = z;
//	}
//}// end routine setPixel
//void drawPoint(int x, int y, double z, float r, float g, float b){
//	int size = 0;
//	if (isSizedPts){
//		setPixel(x, y, z);
//		int candidate = z_buffer[y][x] * 10;
//		switch (candidate){
//		case 9:
//		case 8:
//		case 7:
//			size = 0;
//		case 6:
//		case 5:
//		case 4:
//			size = 1;
//		case 3:
//		case 2:
//			size = 2;
//		case 1:
//		case 0:
//			size = 3;
//		}
//	}
//	if (size > 0 || isSizedPts){
//		for (int i = x - size; i < x + size; i++){
//			if (i < 0) continue;
//			for (int j = y - size; j < y + size; j++){
//				if (j < 0) continue;
//				if (!(x < window_width && x >= 0 && y >= 0 && y < window_height)) {
//					return;
//				}
//				else if (!isSizedPts){
//					drawPixels(i, j, r, g, b);
//				}
//				else{
//					setPixel(Vector3(i, j, z), Vector3(r, g, b));
//				}
//			}
//		}
//		setPixel(Vector3(x, y, z), Vector3(r, g, b));
//	}
//	else{
//		drawPixels(x, y, r, g, b);
//	}
//}
//
//void setPixel(Vector3 point, Vector3 color){
//	_ASSERT(0 <= point.z() && point.z() <= 1);
//	if (point.y() >= window_height || point.x() >= window_width) return;
//
//	if (point.z() < z_buffer[point.y()][point.x()]){
//		z_buffer[point.y()][point.x()] = point.z();
//		drawPixels(point.x(), point.y(), color.x(), color.y(), color.z());
//	}
//}// end routine setPixel
//
//Vector3 light_point(10, 10, 0);
//Matrix4 light_point_matrix;
//Matrix4 p2w;
//Matrix4 D;
//Camera C;
//Matrix4 P;
//bool isShading = false, isRotate = false, isLightRotate = false, isBuffer = false;
//
//void rasterize()
//{
//	// Put your main rasterization loop here
//	// It should go over the point model and call drawPoint for every point in it
//	z_buffer.clear();
//	z_buffer.reserve(window_height);
//	for (int i = 0; i < window_height; i++){
//		z_buffer.push_back(vector<double>(window_width, 1));
//	}
//
//	for (int i = 0; i < M->count(); i++){
//		Vector4 p = M->getVerts(i);
//		Vector4 n = M->getNorms(i).toVector4(0);
//		p = (M->getMatrix() * p); // convert points to the World Coords
//		n = (M->getMatrix() * n); // convert the normals into the World Coords
//		n.internalNormalize();
//
//		//--- insert shading calculation here
//		Vector3 R_c(1.0, 1.0, 1.0);
//		if (isShading){
//			R_c = shaderizing(p.toVector3(), n.toVector3(), Vector3(1.0, 1.0, 1.0), 1000);
//		}
//		//--- end of shading cacluation
//
//		//--- World to Camera
//		p = (C.inverse() * p);
//
//		//--- Camera to Perspective
//		p = (P * p);
//		p.dehomogenize();
//
//		//		printf("%f\n", p.z());
//
//		p = (D * p);
//		//--- finally draw this point to the screen
//		if (p.x() < window_width && p.x() >= 0 && p.y() >= 0 && p.y() < window_height) {
//			if (isBuffer && !isSizedPts){
//				setPixel(p.toVector3(), R_c);
//			}
//			else{
//				drawPoint(p.x(), p.y(), p.z(), R_c.x(), R_c.y(), R_c.z());
//			}
//		}
//	}
//}// end routine p=MCPDp
//
//Vector3 shaderizing(const Vector3& p, const Vector3& norm, Vector3 l_colr, double intensity){
//	//--- light reflection calculation
//	Vector3 L_p = (light_point_matrix * light_point.toVector4(1)).toVector3();
//	//L_p.print("L_p:");
//	//p.print("p");
//	Vector3 L_d = L_p - p;
//	//L_d.print("L_d = L_p - p");
//	double distance = L_d.length(); // cal distance btwn vectors |V-M|
//	//printf("Distance: %f\n", distance);
//	L_d.normalize();
//	//L_d.print("L_d normalized: ");
//	//printf("L_d normalized distance: %f\n", L_d.length());
//
//	//Vector3 M_c(norm.x(), norm.y(), norm.z()); // The material's actual color
//	Vector3 M_c(1.0, 1.0, 1.0); // surface color of object
//	Vector3 color(1.0, 1.0, 1.0);
//	Vector3 L_i = l_colr.scale(intensity); // The light intensity
//	Vector3 L_c = L_i / (distance * distance);
//
//	double light_norm = L_d.dot(norm);
//	//if (light_norm != light_norm) return Vector3(1.0, 1.0, 1.0);
//
//	Vector3 R_c = (M_c * L_c).scale((light_norm) / M_PI);
//	//---
//	return R_c;
//}
//
//// Sets up the point where we will be drawing the actual points onto the screen
//void cpuViewport(int x, int y, long width, long height){
//	D.insert(0, 0, (width - x) / 2.0);
//	D.insert(0, 3, (x + width) / 2.0);
//	D.insert(1, 1, (height - y) / 2.0);
//	D.insert(1, 3, (y + height) / 2.0);
//	D.insert(2, 2, 0.5);
//	D.insert(2, 3, 0.5);
//	D.insert(3, 3, 1.0);
//	//D.print("Viewport matrix: ");
//}// end routine viewportFunction
//
//void cpuPerspective(double angle, double aspect, double near_p, double far_p){
//	double pts = tan((angle / 2) * M_PI / 180);
//	P.insert(0, 0, (1.0 / (aspect * pts)));
//	P.insert(1, 1, (1.0 / pts));
//	P.insert(2, 2, ((near_p + far_p) / (near_p - far_p)));
//	P.insert(2, 3, (2.0 * near_p * far_p) / (near_p - far_p));
//	P.insert(3, 2, -1.0);
//	P.print("Projection matrix: ");
//}
//
//// Called whenever the window size changes
//void reshapeCallback(int new_width, int new_height)
//{
//	window_width = new_width;
//	window_height = new_height;
//
//	//---
//	cpuViewport(0, 0, new_width, new_height);
//	cpuPerspective(60.0, double(new_width) / double(new_height), 1.0, 1000.0);
//	//---
//
//	z_buffer_init(new_width, new_height);
//
//	delete[] pixels;
//	pixels = new float[window_width * window_height * 3];
//	displayCallback();
//}
//
//int delta = 5;
//void keyboardCallback(unsigned char key, int, int)
//{
//	cerr << "Key pressed: " << key << endl;
//	switch (key){
//	case '6':
//	case '5':
//	case '4':
//		//--- turn on varied point sizes
//		isSizedPts = true;
//		isShading = true;
//		isBuffer = true;
//		break;	
//	case '3':
//		//--- turn on z-buffering
////		isSizedPts = false;
////		isShading = !isShading;
//		isBuffer = !isBuffer;
//		break;
//	case '2':
//		//--- turn on lighting and shading
////		isSizedPts = false;
//		isShading = !isShading;
////		isBuffer = false;
//		break;
//	case '1':
//		isSizedPts = false;
//		isShading = false;
//		isBuffer = false;
//		break;
//	case 'z':
//		// rotate z
//		break;
//	case 'Z':
//		break;
//	case 'x':
//		// rotate x
//		break;
//	case 'X':
//		break;
//	case 'c':
//		// rotate y
//		break;
//	case 'r':
//		isRotate = !isRotate;
//		break;
//	case 'C':
//		break;
//	case 's':{
//		Matrix4 temp;
//		temp.makeScale(1.1, 1.1, 1.1);
//		M->getMatrix() = temp * M->getMatrix();
//		break;
//	}
//	case 'S':{
//		Matrix4 temp;
//		temp.makeScale(0.9, 0.9, 0.9);
//		M->getMatrix() = temp * M->getMatrix();
//		break;
//	}
//	case 't':
//		delta = -delta;
//		break;
//	case 'k':
//		isLightRotate = !isLightRotate;
//		printf("rotate light\n");
//		break;
//	case 'm':
//		break;
//	case 'M':
//		break;
//	case ',':
//		break;
//	case '<':
//		break;
//	case '.':
//		break;
//	case '>':
//		break;
//	default:
//		break;
//	}
//}
//
//void specKeyboardCallback(int key, int x, int y){
//	printf("Key pressed: ");
//	switch (key){
//	case GLUT_KEY_F1:
//		printf("F1\n");
//		M = &rabbit;
//		break;
//	case GLUT_KEY_F2:
//		printf("F2\n");
//		M = &dragon;
//		break;
//	default:
//		printf("Who cares");
//		break;
//	}
//}
//
//void idleCallback(){
//	if (isRotate){
//		M->getMatrix().makeRotateY_f(delta);
//	}
//	if (isLightRotate){
//		light_point_matrix.makeRotateY_b(10);
//	}
//	displayCallback();
//}
//
//void displayCallback()
//{
//	//---
//	//cpuLoadMatrix(M->getMatrix(), C.inverse());
//	//---
//
//	clearBuffer();
//	rasterize();
//
//	//---
//
//	//---
//
//	// glDrawPixels writes a block of pixels to the framebuffer
//	glDrawPixels(window_width, window_height, GL_RGB, GL_FLOAT, pixels);
//
//	glutSwapBuffers();
//}
//
////
//int main(int argc, char** argv) {
//	glutInit(&argc, argv);
//
//	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
//	glutInitWindowSize(window_width, window_height);
//	glutCreateWindow("Rasterizer");
//
//	loadData();
//	//---
//	cpuViewport(0, 0, window_width, window_height);
//	double aspect = window_width / window_height;
//	cpuPerspective(60.0, aspect, 1.0, 1000.0);
//	C.set(Vector3(0, 0, 20), Vector3(0, 0, 0), Vector3(0, 5, 0));
//	light_point_matrix.identity();
//	z_buffer_init(window_width, window_height);
//	//---
//
//	glutReshapeFunc(reshapeCallback);
//	glutDisplayFunc(displayCallback);
//	glutIdleFunc(idleCallback);
//	glutKeyboardFunc(keyboardCallback);
//	glutSpecialFunc(specKeyboardCallback);
//	glutMainLoop();
//
//	//--- object creation
//}
