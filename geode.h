#ifndef _GEODE_H_
#define _GEODE_H_

#include "node.h"

class Geode : public Node{
private:
	
public:
	//--- constructor
	//--- routines
	void draw(Matrix4 C);
	virtual void update() = 0;
	virtual void render() = 0;
	//--- extraenous
};

#endif