#ifndef _CAMERA_H_
#define _CAMERA_H_
#include <iostream>
#include <GL/glut.h>

#include "Matrix4.h"
#include "Vector3.h"

#define MAT_SIZE 16

class Camera{
protected:
	// Points
	Vector3 e;  //--- center of projection
	Vector3 d;  //--- look at point
	Vector3 up; //--- up vector

	// Matrix
	Matrix4 C;  //--- internal camera matrix
	GLdouble t[MAT_SIZE];

	void update();
public:
	// Constructor
	Camera();
	Camera(Vector3 c, Vector3 d, Vector3 up);

	// Routines 	
	GLdouble * getGLMatrix();
	void set(Vector3 projection, Vector3 lookat, Vector3 up);
	Matrix4 inverse();
	Matrix4& getMatrix(){
		return C;
	}
	//*********
	Vector3 getEyePos(void);
};
#endif