#include <math.h>
#include <iostream>

#include "Vector4.h"

// defaults to a point at 0,0,0
Vector4::Vector4() : Vector4(0, 0, 0, 1) {}
Vector4::Vector4(float x, float y, float z, float w)
{
	v[0] = x;
	v[1] = y;
	v[2] = z;
	v[3] = w;
} 

Vector4::~Vector4(){
//	delete v[0];
}

void Vector4::print(string comment) const
{
	cout << comment << "< " << v[0] << ", " << v[1] << ", " << v[2] << ", " << v[3] << " >" << endl;
}

string Vector4::toString() const
{
	return "< " + to_string(v[0]) + ", " + to_string(v[1]) + ", " + to_string(v[2]) + ", " + to_string(v[3]) + " >";
}

Vector3 Vector4::toVector3(){
	return Vector3(v[0], v[1], v[2]);
}

void Vector4::internalNormalize(){
	Vector3 temp = toVector3();
	temp.normalize();
	float * t = temp.getPointer();
	for (int i = 0; i < 3; i++){
		v[i] = t[i];
	}
}

Vector4 Vector4::operator+(const Vector4& other)
{
	float x = v[0] + other.v[0];
	float y = v[1] + other.v[1];
	float z = v[2] + other.v[2];
	float w = v[3] + other.v[3];
	return Vector4(x, y, z, w);
}

Vector4 Vector4::operator-(const Vector4& other)
{
	float x = v[0] - other.v[0];
	float y = v[1] - other.v[1];
	float z = v[2] - other.v[2];
	float w = v[3] - other.v[3];

	return Vector4(x, y, z, w);
}

bool Vector4::operator==(const Vector4 & other){
	if (this == &other){
		return true;
	}
	return this->x() == other.x() &&
				 this->y() == other.y() &&
		     this->z() == other.z();
}

void Vector4::dehomogenize()
{
	float w = v[3];
	v[3] = 1;
	
	if (w == 0) {
		return;
	}

	v[0] /= w;
	v[1] /= w;
	v[2] /= w;
}

float Vector4::x() const
{
	return v[0];
}

float Vector4::y() const
{
	return v[1];
}

float Vector4::z() const
{
	return v[2];
}

float Vector4::w() const
{
	return v[3];
}

float * Vector4::getPointer(){
	return v;
}

float Vector4::length(){
	return sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
}

Vector4 internalCross(const Vector4 & other){
	//other.toVector3();
	Vector4 v;
	return v;
}

void Vector4::scale(const float& dval)
{
	v[0] *= dval;
	v[1] *= dval;
	v[2] *= dval;
	v[3] *= dval;
}

