#ifndef _LIGHT_H_
#define _LIGHT_H_

#include "Matrix4.h"
#include "geode.h"
//#include "GLee.h"


enum Type{ POINTV, SPOT, DIRECTIONAL };

class Light : public Geode {
private:
	Vector4 lightPos;
	Vector3 spotDirection;
	float * light_ambient;
	float * light_diffuse;
	float * light_specular;
	float * light_pos;
	float cut_off;
	float * spot_direction;
	int GL_LIGHT_NUM;
	Type TYPE;

	Matrix4 m2w;
public:

	Light(Type TYPE, int GL_LIGHT_NUM, float l_ambient[4], float l_diffuse[4], float l_specular[4], float l_pos[4], float cut_off, float spot_direction[4]):
		TYPE(TYPE), GL_LIGHT_NUM(GL_LIGHT_NUM), light_ambient(l_ambient), light_diffuse(l_diffuse), light_specular(l_specular), light_pos(l_pos),
		cut_off(cut_off), spot_direction(spot_direction){
		m2w.makeTranslate(l_pos[0], l_pos[1], l_pos[2]);
	}

	void draw(Matrix4 C){
		Geode::draw(C);
		render();
	}

	void update(){

	}

	void render(){
		switch (TYPE){
		case POINTV:{
			glDisable(GL_LIGHT_NUM);
			glLightfv(GL_LIGHT_NUM, GL_AMBIENT, light_ambient);
			glLightfv(GL_LIGHT_NUM, GL_DIFFUSE, light_diffuse);
			glLightfv(GL_LIGHT_NUM, GL_SPECULAR, light_specular);
			glLightfv(GL_LIGHT_NUM, GL_POSITION, light_pos);
			glEnable(GL_LIGHT_NUM);

			break;
		}
		case SPOT:{
			glLightfv(GL_LIGHT_NUM, GL_AMBIENT, light_ambient);
			glLightfv(GL_LIGHT_NUM, GL_DIFFUSE, light_diffuse);
			glLightfv(GL_LIGHT_NUM, GL_SPECULAR, light_specular);
			glLightfv(GL_LIGHT_NUM, GL_POSITION, light_pos);
			glLightf(GL_LIGHT_NUM, GL_SPOT_CUTOFF, cut_off);
			glLightfv(GL_LIGHT_NUM, GL_SPOT_DIRECTION, spot_direction);
			glEnable(GL_LIGHT_NUM);
			break;
		}
		default:
			break;
		}
	}
};

#endif