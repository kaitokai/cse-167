#ifndef _WINDOW_H_
#define _WINDOW_H_
//#include <GL\glew.h>
#include "GLee.h"
#include <iostream>
#include "Robot.h"
#include "XYZParser.h"
#include "lightplay.h"
#include <ctime>
#include <thread>
#include <GL/glut.h>

//#include "Window.h"
#include "main.h"

#include "Camera.h"
#include "shader.h"
#include "Matrix4.h"
#include "sphere.h"
#include "cubic.h"
#include "matrixTransform.h"
#include "Material.h"
#include "BezierCurve.h"

using namespace std;

class Window	  // OpenGL output window related routines
{
private:
	static Camera C;

public:
	enum Mode{ CUBE, HOUSE, POINT };

	static int width, height; 	            // window size

	static void idleCallback(void);
	static void reshapeCallback(int, int);
	static void displayCallback(void);
	static void keyboardCallback(unsigned char, int, int);
	static void specKeyboardCallback(int key, int x, int y);
	static void mouseCallback(int button, int state, int x, int y);
	static void motionHandler(int x, int y);

	static Vector3 trackballMapping(Vector3 point);

	//--- do not include the file extension
	static void initializeShader(int & id, const char * filename, size_t size);
};
#endif
//
