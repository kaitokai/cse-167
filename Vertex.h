#ifndef _VERTEX_H_
#define _VERTEX_H_
#include "Vector3.h"
#include "Vector4.h"

class Vertex{
private:
	Vector4 pos;      // position vertex
	Vector3 normls;   // surface normals attributed to the point position
	Vector3 colrs;    // surface material color
//	Vector3 texcrds;  // texture coordinates
public:
	//--- Constructors
	Vertex();
	Vertex(const Vector4 & pos, const Vector3 & normls, const Vector3 & colrs);
	//--- Destructors
	~Vertex();
	//--- Accessors/Mutators
	void setPos(Vector4 pos);

	const Vector4 getPos(){
		return pos;
	}

	const Vector3 getColors(){
		return colrs;
	}

	const Vector3 getNormals(){
		return normls;
	}

	void setTexCoor();
	//--- Routines
};

#endif
