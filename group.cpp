#include "group.h"

void Group::draw(Matrix4 C){
	for (list<Node*>::iterator iter = children.begin(); iter != children.end(); iter++){
		(*iter)->draw(C);
	}
}// end routine draw

void Group::update(){
	for (list<Node*>::iterator iter = children.begin(); iter != children.end(); iter++){
		(*iter)->update();
	}
}// end update

bool Group::addChild(Node& child){
	children.push_back(&child);
	return true;
}// end routine addChild

bool Group::removeChild(Node& child){
	children.remove(&child);
	return true;
}// end routine removeChild

bool Group::removeAll(){
	children.clear();
	return true;
}

//--- EOD ---//