#include "Robot.h"

//--- begin constructors
Robot::Robot(){
	const double SIZE = 1;
	Matrix4 _temp;
	_temp.makeScale(0.75, 0.75, 0.75);
	MatrixTransform * _root = new MatrixTransform(_temp);
	MatrixTransform * _locat, * t_locat;

	//--- hed
	Geode * shapes;
	shapes = new Cubic(SIZE);
	_root->addChild(*shapes);

	//--- torso
	shapes = new Cubic(SIZE);
	_temp.makeScale(2.5, 1.25, 1.2);
	_temp.makeTranslate_f(0, -1.3, 0);
	helper(_root, _temp, *shapes);

	//--- vents
	shapes = new Cubic(SIZE);
	_temp.makeScale(.5, 1.5, 1.5);
	_temp.makeTranslate_f(-1.1, -1.3, 0);
	helper(_root, _temp, *shapes);

	shapes = new Cubic(SIZE);
	_temp.makeScale(.5, 1.5, 1.5);
	_temp.makeTranslate_f(1.1, -1.3, 0);
	helper(_root, _temp, *shapes);
	//--- end vents

	shapes = new Cubic(SIZE);
	_temp.makeScale(1.4, 1.75, 1);
	_temp.makeTranslate_f(0, -2.6, 0);
	helper(_root, _temp, *shapes);

	shapes = new Cubic(SIZE);
	_temp.makeScale(2, 1, 1);
	_temp.makeTranslate_f(0, -4, 0);
	helper(_root, _temp, *shapes);

	//--- shoulder pads
	//--- LEFT ARM SEGMENT
	_temp.makeTranslate(1.9, 0, 0);
	MatrixTransform * _leftArm = new MatrixTransform(_temp);

	shapes = new Cubic(SIZE);
	_temp.makeScale(1, 2.5, 1);
	helper(_leftArm, _temp, *shapes);

	shapes = new Cubic(SIZE);
	_temp.makeScale(0.7, 2, 0.7);
	_temp.makeTranslate_f(0, -2.3, 0);
	helper(_leftArm, _temp, *shapes);

	shapes = new Cubic(SIZE);
	_temp.makeScale(1, 2, 1);
	_temp.makeTranslate_f(0, -4.4, 0);
	helper(_leftArm, _temp, *shapes);

	leftArm = new MatrixTransform(Matrix4::I());
	leftArm->addChild(*_leftArm);

	_root->addChild(*leftArm);
	//--- END LEFT ARM SEGMENT

	//--- RIGHT ARM SEGMENT
	_temp.makeTranslate(-1.9, 0, 0);
	MatrixTransform * _rightArm = new MatrixTransform(_temp);
	shapes = new Cubic(SIZE);
	_temp.makeScale(1, 2.5, 1);
	helper(_rightArm, _temp, *shapes);

	shapes = new Cubic(SIZE);
	_temp.makeScale(0.7, 2, 0.7);
	_temp.makeTranslate_f(0, -2.3, 0);
	helper(_rightArm, _temp, *shapes);

	shapes = new Cubic(SIZE);
	_temp.makeScale(1, 2, 1);
	_temp.makeTranslate_f(0, -4.4, 0);
	helper(_rightArm, _temp, *shapes);

	rightArm = new MatrixTransform(Matrix4::I());
	rightArm->addChild(*_rightArm);
	_root->addChild(*rightArm);
	//--- END RIGHT ARM SEGMENT

	//--- RIGHT LEG SEGMENT
	_temp.makeTranslate(-.6, 0, 0);
	MatrixTransform * _rightLeg = new MatrixTransform(_temp);

	shapes = new Cubic(SIZE);
	_temp.makeScale(.7, 2, 0.7);
	_temp.makeTranslate_f(0, -5.7, 0);
	helper(_rightLeg, _temp, *shapes);

	_temp.makeTranslate(-.6, -8, 0);
	rightKnee = new MatrixTransform(_temp);
	shapes = new Cubic(SIZE);
	_temp.makeScale(1, 2.5, 1.4);
	helper(rightKnee, _temp, *shapes);
//	_rightLeg->addChild(*rightKnee);

	rightLeg = new MatrixTransform(Matrix4::I());
	rightLeg->addChild(*_rightLeg);
	rightLeg->addChild(*rightKnee);

	_root->addChild(*rightLeg);
	//--- END RIGHT LEG SEGMENT

	//--- LEFT LEG SEGMENT
	_temp.makeTranslate(.6, 0, 0);
	MatrixTransform * _leftLeg = new MatrixTransform(_temp);

	shapes = new Cubic(SIZE);
	_temp.makeScale(.7, 2, 0.7);
	_temp.makeTranslate_f(0, -5.7, 0);
	helper(_leftLeg, _temp, *shapes);

	_temp.makeTranslate(.6, -8, 0);
	leftKnee = new MatrixTransform(_temp);
	shapes = new Cubic(SIZE);
	_temp.makeScale(1, 2.5, 1.4);
	_temp.makeTranslate_f(0, 0, 0);
	helper(leftKnee, _temp, *shapes);

	leftLeg = new MatrixTransform(Matrix4::I());
	leftLeg->addChild(*_leftLeg);
	leftLeg->addChild(*leftKnee);

	_root->addChild(*leftLeg);
	//--- END LEFT LEG SEGMENT
	root = (_root);
}
//--- end constructors

//--- begin routines
double angle = 0;
void Robot::build(Matrix4 C){
	glColor3f(1, 0, 0);
//	l_arm->getM().makeRotateZ_f((degs < 90)?degs++:degs--);
	leftArm->getM().makeTranslate_f(0, 1, 0);
	leftArm->getM().makeRotateX_f((sin((angle - 90)*M_PI/180) < 0)?-0.05:0.05);
	leftArm->getM().makeTranslate_f(0, -1, 0);
	
	rightArm->getM().makeTranslate_f(0, 1, 0);
	rightArm->getM().makeRotateX_f((sin((angle + 90)*M_PI / 180) < 0) ? -0.05 : 0.05);
	rightArm->getM().makeTranslate_f(0, -1, 0);

	rightLeg->getM().makeTranslate_f(0, 4, 0);
	rightLeg->getM().makeRotateX_f((sin((angle + 90)*M_PI / 180) <= 0) ? 0.05 : -0.05);
	rightLeg->getM().makeTranslate_f(0, -4, 0);

	rightKnee->getM().makeTranslate_f(0, 6, 0);
	rightKnee->getM().makeRotateX_f((sin((angle + 175 + 45)*M_PI / 180) <= 0) ? -0.05 : 0.05);
	rightKnee->getM().makeTranslate_f(0, -6, 0);

	leftLeg->getM().makeTranslate_f(0, 4, 0);
	leftLeg->getM().makeRotateX_f((sin((angle + 90 + 180)*M_PI / 180) < 0) ? 0.05 : -0.05);
	leftLeg->getM().makeTranslate_f(0, -4, 0);

	leftKnee->getM().makeTranslate_f(0, 6, 0);
	leftKnee->getM().makeRotateX_f((sin((angle - 175)*M_PI / 180) <= 0) ? -0.05 : 0.05);
	leftKnee->getM().makeTranslate_f(0, -6, 0);

	angle += 0.1;

	root->update();
	root->draw(C);
}

void Robot::helper(MatrixTransform * _root, Matrix4& trsfm, Geode & shpe){
	MatrixTransform * _locat = new MatrixTransform(trsfm);
	_locat->addChild(shpe);
	_root->addChild(*_locat);
}

void Robot::jointManip(JOINT j, Matrix4 mat){

}
//--- end routines

//--- EOD ---//