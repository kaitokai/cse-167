#include "Window.h"

using namespace std;

//--- PROTOTYPES
void loadTexture(const char * path, GLuint texture);
//GLuint texture[5];

//--- END PROTOTYPES

int Window::width = 912;   // set window width in pixels here
int Window::height = 912;   // set window height in pixels here
//--- for initializations
bool isInitialized = false;


//************************************
bool shadowMap = true;

GLuint shadowMap_texture;
const int shadowMap_size = 512;

Vector3 light_pos = Vector3(-6.0f, -15.0f, 17.0f);    //1.0, 3.0, 1.0 //1, 20, 25
Vector4 col_white = Vector4(1, 1, 1, 0);
Vector4 col_black = Vector4(0, 0, 0, 0);
Matrix4 m_lightProj;
Matrix4 m_camProj;
Matrix4 m_lightView;
Matrix4 m_camView;

//static Camera camera;
float xcoord = -1.5;
float ycoord = 2.5;
float zcoord = 3;
float camOffset = -2.5;

Vector3 center = Vector3(xcoord, ycoord, zcoord);
Vector3 lookAt = Vector3(0, 0, 0);
Vector3 upVec = Vector3(0, 1, 0);


//****************************************

Window::Mode mode = Window::Mode::CUBE;
int delta = 0;

//--- for PA6
bool isWaveMotion = true;

GLuint texture[5];
bool isShaderPosFree[10]; // boolean array to turn off and on shader programs
bool isShaderEnabled[10];
Shader shadersArr[10]; // fuck you


int gradiance = 10;
Bez b = Bez(Vector4(0, 0, 0, 1), Vector4(3, 3, 0, 1), Vector4(3, 3, 0, 1), Vector4(6, 0, 0, 1), gradiance);
Bez bb = Bez(Vector4(6, 0, 0, 1), Vector4(7, 3, 0, 1), Vector4(8, -3, 0, 1), Vector4(9, 0, 0, 1), gradiance);
Bez bbb = Bez(Vector4(9, 0, 0, 1), Vector4(10, 3, 3, 1), Vector4(11, -3, 5, 1), Vector4(12, -7, 10, 1), gradiance);
BezCombinator bezC;

int mat_mode = 0;
Matrix4 testMatrix;

//---
int silhouetteID;
int colrsID;
//---
Light * l;
int param = 0;

//----------------------------------------------------------------------------
// Callback method called when system is idle.
void Window::idleCallback()
{
 
	if (!isInitialized ){
		float * ambient = new float[]{.1f, 0, 0, 1};
		float * diffuse = new float[]{1, 1, 1, 1};
		float * specular = new float[]{1, 1, 1, 1};
		float * lightPos = new float[]{1, -10, 5, 1};
		float cut_off = 10;
		float * spot_pos = new float[]{-1, -1, 0};
		l = new Light(Type::POINTV, GL_LIGHT1, ambient, diffuse, specular, lightPos, cut_off, spot_pos);

		//--- load up all the shaders
		initializeShader(silhouetteID, "silhouette", 11); // get the silhouette program up
		initializeShader(colrsID, "simpleclrs", 11);


		testMatrix.identity();
		testMatrix.makeRotateY_b(180);

		//--- bezier stuff
		bezC.addBez(b);
		bezC.addBez(bb, 1);
		bezC.addBez(bbb, 1);

		//--- stop anything from initializing more than once
		isInitialized = true;
	}
	
	displayCallback();         // call display routine to show the cube
}

//----------------------------------------------------------------------------
// Callback method called by GLUT when graphics window is resized by the user
void Window::reshapeCallback(int w, int h)
{
	cerr << "Window::reshapeCallback called" << endl;
	width = w;
	height = h;
	glViewport(0, 0, w, h);  // set new viewport size
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60.0, double(width) / (double)height, 1.0, 1000.0); // set perspective projection viewing frustum
//	glTranslatef(0, 0, -20);    // move camera back 20 units so that it looks at the origin (or else it's in the origin)
	Globals::c2w.set(Vector3(0, 0, -20), Vector3(0, 0, 0), Vector3(0, 1, 0));
	//Globals::c2w.getMatrix().makeRotateY_b(180);
	glMatrixMode(GL_MODELVIEW);

}

GLuint LoadTextureRAW(const char * filename, int wrap)
{
	GLuint texture;
	int width, height;
	BYTE * data;
	FILE * file;

	file = fopen(filename, "rb");
	if (file == NULL) return 0;

	width = 256;
	height = 256;
	data = (BYTE*)malloc(width * height * 3);

	fread(data, width * height * 3, 1, file);
	fclose(file);

	glGenTextures(1, &texture);

	glBindTexture(GL_TEXTURE_2D, texture);

	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
		GL_LINEAR_MIPMAP_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,
		wrap ? GL_REPEAT : GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,
		wrap ? GL_REPEAT : GL_CLAMP);

	gluBuild2DMipmaps(GL_TEXTURE_2D, 3, width,
		height, GL_RGB, GL_UNSIGNED_BYTE, data);

	free(data);

	return texture;

}

GLuint grass_texture;

bool InitShadow(void)
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glShadeModel(GL_SMOOTH);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glClearDepth(1.0f);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glGenTextures(1, &shadowMap_texture);
	glBindTexture(GL_TEXTURE_2D, shadowMap_texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, shadowMap_size, shadowMap_size, 0,
		GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
	glEnable(GL_COLOR_MATERIAL);
	glMaterialfv(GL_FRONT, GL_SPECULAR, (GLfloat*) &col_white);
	glMaterialf(GL_FRONT, GL_SHININESS, 11.0f);
	glPushMatrix();
	glLoadIdentity();
	gluPerspective(60.0f, (float)Window::width / Window::height, 1.0f, 1000.0f);
	glGetDoublev(GL_MODELVIEW_MATRIX, (GLdouble*) &m_camProj);
	Vector3 eye = Globals::c2w.getEyePos();
	glLoadIdentity();
	gluLookAt(eye[0], eye[1], eye[2],
		0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f);
	glGetDoublev(GL_MODELVIEW_MATRIX, (GLdouble*)&m_camView);
	glLoadIdentity();
	gluPerspective(60.0f, 1.0f, 1.0f, 1000.0f); 
	glGetDoublev(GL_MODELVIEW_MATRIX, (GLdouble*)&m_lightProj);
	glLoadIdentity();
	gluLookAt(light_pos[0], light_pos[1], light_pos[2],
		0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f);
	glGetDoublev(GL_MODELVIEW_MATRIX, (GLdouble*)& m_lightView);
	glPopMatrix();

	grass_texture = LoadTextureRAW("grass.raw", TRUE);

	return true;

}



void DrawObjects(void)
{
	int i, j;
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);  
	glMatrixMode(GL_MODELVIEW);

	

	Matrix4 m_translate;
	Matrix4 rot_camX, rot_camY, rot_camZ;
		m_translate.makeTranslate(10, 60, -15);
		rot_camX.makeRotateX(150);
		rot_camY.makeRotateY(0);
		rot_camZ.makeRotateZ(170);
	Matrix4 m_cam = Globals::c2w.getMatrix();
	Matrix4 m_modelview;

	m_modelview = m_cam * testMatrix * rot_camZ * rot_camY * rot_camX * m_translate;

	m_modelview.transpose();
	glLoadMatrixd(m_modelview.getPointer());	

	glPushMatrix();
	glColor4f(0.3f, 0.8f, 0.3f, 1);
	glBindTexture(GL_TEXTURE_2D, grass_texture);
	glBegin(GL_QUADS);
	glTexCoord2d(0.0, 0.0); glVertex3f(-150, -150, -50);
	glTexCoord2d(1.0, 0.0); glVertex3f(150, -150, -50);
	glTexCoord2d(1.0, 1.0); glVertex3f(150, 150, -50);
	glTexCoord2d(0.0, 1.0); glVertex3f(-150, 150, -50);
	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);
	glPopMatrix();

	glPushMatrix();
	glColor3f(0.1f, 0.2f, 0.9f);
	glutSolidCube(3);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-10, 15, 1);
	glColor3f(1.4f, 0.46f, 0.1f);
	glutSolidTorus(1, 2, 50, 50);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(5, 15, 5.0);
	glColor3f(0.4f, 0.46f, 0.1f);
	glutSolidSphere(2.4, 14, 14);
	glPopMatrix();

	

/*
	glBindTexture(GL_TEXTURE_2D, grass_texture);

	glPushMatrix();
	glBegin(GL_QUADS);
	glTexCoord2d(0.0, 0.0); glVertex3d(-150.0, -150.0, -50);
	glTexCoord2d(1.0, 0.0); glVertex3d(150.0, -150.0, -50);
	glTexCoord2d(1.0, 1.0); glVertex3d(150.0, 150.0, -50);
	glTexCoord2d(0.0, 1.0); glVertex3d(-150.0, 150.0, -50);
	glEnd();
	glPopMatrix();
*/
}


void drawWithShadows()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_PROJECTION);
	glLoadMatrixd((GLdouble*)&m_lightProj);
	glMatrixMode(GL_MODELVIEW);
	glLoadMatrixd((GLdouble*)&m_lightView);
	glViewport(0, 0, shadowMap_size, shadowMap_size);
	glCullFace(GL_FRONT);
	glShadeModel(GL_FLAT);
	glColorMask(0, 0, 0, 0);

	DrawObjects();

	glBindTexture(GL_TEXTURE_2D, shadowMap_texture);
	glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 0, 0, shadowMap_size, shadowMap_size);
	glCullFace(GL_BACK);
	glShadeModel(GL_SMOOTH);
	glColorMask(1, 1, 1, 1);
	glClear(GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_PROJECTION);
	glLoadMatrixd((GLdouble*)&m_camProj);
	glMatrixMode(GL_MODELVIEW);
	glLoadMatrixd((GLdouble*)&m_camView);
	glViewport(0, 0, Window::width, Window::height);
	col_white.scale(0.2);
	glLightfv(GL_LIGHT1, GL_POSITION, (GLfloat*)&light_pos);
	glLightfv(GL_LIGHT1, GL_AMBIENT, (GLfloat*)&col_white);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, (GLfloat*)&col_white);
	glLightfv(GL_LIGHT1, GL_SPECULAR, (GLfloat*)&col_black);
	glEnable(GL_LIGHT1);
	glEnable(GL_LIGHTING);

	DrawObjects();

	col_white.scale(5);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, (GLfloat*)&col_white);
	glLightfv(GL_LIGHT1, GL_SPECULAR, (GLfloat*)&col_white);
	static Matrix4 m_bias(0.5, 0.0, 0.0, 0.0,
		0.0, 0.5, 0.0, 0.0,
		0.0, 0.0, 0.5, 0.0,
		0.5, 0.5, 0.5, 1.0);
	Matrix4 m_texture;
	m_bias.transpose();
	m_lightProj.transpose();
	m_lightView.transpose();
	m_texture.identity();
	m_texture = m_texture.mult(m_bias);
	m_texture = m_texture.mult(m_lightProj);
	m_texture = m_texture.mult(m_lightView);
	m_texture.transpose();
	m_bias.transpose();
	m_lightProj.transpose();
	m_lightView.transpose();

	glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_EYE_LINEAR);
	glTexGenfv(GL_S, GL_EYE_PLANE, m_texture.getCol(0).getPointer());
	glEnable(GL_TEXTURE_GEN_S);
	glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_EYE_LINEAR);
	glTexGenfv(GL_T, GL_EYE_PLANE, m_texture.getCol(1).getPointer());
	glEnable(GL_TEXTURE_GEN_T);
	glTexGeni(GL_R, GL_TEXTURE_GEN_MODE, GL_EYE_LINEAR);
	glTexGenfv(GL_R, GL_EYE_PLANE, m_texture.getCol(2).getPointer());
	glEnable(GL_TEXTURE_GEN_R);
	glTexGeni(GL_Q, GL_TEXTURE_GEN_MODE, GL_EYE_LINEAR);
	glTexGenfv(GL_Q, GL_EYE_PLANE, m_texture.getCol(3).getPointer());
	glEnable(GL_TEXTURE_GEN_Q);
	glBindTexture(GL_TEXTURE_2D, shadowMap_texture);
	glEnable(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE_ARB, GL_COMPARE_R_TO_TEXTURE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC_ARB, GL_LEQUAL);
	glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE_ARB, GL_INTENSITY);
	glAlphaFunc(GL_GEQUAL, 0.99f);
	glEnable(GL_ALPHA_TEST);

	DrawObjects();

	glDisable(GL_TEXTURE_2D);
	glDisable(GL_TEXTURE_GEN_S);
	glDisable(GL_TEXTURE_GEN_T);
	glDisable(GL_TEXTURE_GEN_R);
	glDisable(GL_TEXTURE_GEN_Q);
	glDisable(GL_LIGHTING);
	glDisable(GL_ALPHA_TEST);
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(-10.0f, 10.0f, -10.0f, 10.0f);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
	glFinish();
	glutSwapBuffers();
	glutPostRedisplay();


}

//----------------------------------------------------------------------------
// Callback method called by GLUT when window readraw is necessary or when glutPostRedisplay() was called.
//XYZParser pars = XYZParser("ObjectModels/bunny.obj", 'o');

void Window::displayCallback()
{
	if (shadowMap)
	{
		drawWithShadows();
	}
	else
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);  // clear color and depth buffers

		glMatrixMode(GL_MODELVIEW);

		Matrix4 _temp = Globals::c2w.getMatrix();
		//Matrix4 _temp2 = pars.getMatrix();
		Matrix4 _temp3;
		//_temp3 = (_temp * _temp2) * testMatrix;

		_temp3 = _temp * testMatrix;

		Matrix4 _motionTest = _temp3;
		//	Vector4 _motionLocat = b.curveAt(delta);

		Vector4 _motionLocat = bezC.traverseBy(delta);
		_motionTest.makeTranslate_b(_motionLocat.toVector3());
		bezC.draw(_temp);

		//glLoadMatrixd(Globals::c2w.getGLMatrix());
		//glBegin(GL_QUADS); {
		//	glColor3d(1, 1, 1);
		//	glVertex3d(-10, 10, 5);
		//	glVertex3d(10, 10, 5);
		//	glVertex3d(10, -10, 5);
		//	glVertex3d(-10, -10, 5);
		//}glEnd();

		l->draw(Matrix4::I());

		//_temp3.transpose();
		//glLoadMatrixd(_temp3.getPointer());
		glDisable(GL_CULL_FACE);
		_motionTest.transpose();
		glLoadMatrixd(_motionTest.getPointer());

		if (isShaderEnabled[silhouetteID]){
			shadersArr[silhouetteID].bind();
			GLint camPos = glGetUniformLocationARB(shadersArr[silhouetteID].getPid(), "cameraPos");
			Vector3 __pos = Globals::c2w.getMatrix().getPosition();
			glUniform4fARB(camPos, __pos.x(), __pos.y(), __pos.z(), 0.0f);

			GLint colrs = glGetUniformLocationARB(shadersArr[silhouetteID].getPid(), "colors");
			glUniform3fARB(colrs, 1, 0, 0);
		}

		glColor3d(1, 0, 0);
		glutSolidTeapot(1);

		//pars.draw(_temp3);
		if (isShaderEnabled[silhouetteID]){
			shadersArr[silhouetteID].unbind();
		}

		glEnable(GL_CULL_FACE);


		//--- clear EVERYTHING out for this buffer and go to the new one
		glFlush();
		glutSwapBuffers();
	}
		

}

//bool isShaderOn = false;
void Window::keyboardCallback(unsigned char key, int x, int y)
{
	Matrix4 * c2wm = &(Globals::c2w.getMatrix());
	printf("Key Press: %c\n", key);
	switch (key){
	case 'w':
		//c2wm->makeTranslate_f(0.0, 0.0, 1.0);
//		mtx.getM().makeTranslate_f(0.0, 0.0, 1.0);
			testMatrix.makeTranslate_f(0.0, 0.0, 1.0);
		break;
	case 'a':
		//c2wm->makeTranslate_f(1.0, 0.0, 0.0);
//		mtx.getM().makeTranslate_f(1.0, 0.0, 0.0);
		testMatrix.makeTranslate_f(1.0, 0.0, 0.0);
		break;
	case 'r':
		//c2wm->makeTranslate_f(0.0, 0.0, -1.0);
//		mtx.getM().makeTranslate_f(0.0, 0.0, -1.0);
		testMatrix.makeTranslate_f(0.0, 0.0, -1.0);
		break;
	case 's':
		//c2wm->makeTranslate_f(-1.0, 0.0, 0.0);
//		mtx.getM().makeTranslate_f(-1.0, 0.0, 0.0);
		testMatrix.makeTranslate_f(-1.0, 0.0, 0.0);
		break;
	case 'q':
		//c2wm->makeTranslate_f(0.0, 1.0, 0.0);
		testMatrix.makeTranslate_f(0.0, -1.0, 0.0);
		break;
	case 'f':
		//c2wm->makeTranslate_f(0.0, -1.0, 0.0);
		testMatrix.makeTranslate_f(0.0, 1.0, 0.0);
		break;
	case 't':
		//c2wm->makeRotateY_f(5);
		testMatrix.makeRotateY_b(5);
		break;
	case 'T':
		//c2wm->makeRotateY_f(-5);
		testMatrix.makeRotateY_b(-5);
		break;
	case 'k':	
		shadowMap = true;
		Globals::c2w.set(center, lookAt, upVec);
		InitShadow();
		cout << "shadow in on" << endl;
		break;
	case 'K':
		cout << "shadow in off" << endl;
		Globals::c2w.set(Vector3(0, 0, -20), Vector3(0, 0, 0), Vector3(0, 1, 0));
		shadowMap = false;
		break;
	case 'b':
		//--- turn the bounding-sphere grid on
		break;
	case 'p':
		Globals::isPointsDrawn = !Globals::isPointsDrawn;
		break;
	case 'P':
		Globals::areControlPointsDrawn = !Globals::areControlPointsDrawn;
		break;
	case 'e':
		Globals::isEnvOn = !Globals::isEnvOn;
		break;
	case 'v':
		isShaderEnabled[silhouetteID] = !isShaderEnabled[silhouetteID];
		break;
	case 'c':
		isShaderEnabled[colrsID] = !isShaderEnabled[colrsID];
		break;
	case 'Z':
		if (0 < delta){
			delta--;
		}
		cout << delta << endl;
		break;
	case 'z':
		if (delta < bezC.getGranularity()){
			delta++;
		}
		cout << delta << endl;
		break;
	case '1':
		if(!glIsEnabled(GL_LIGHT0)){
			glEnable(GL_LIGHT0);
		}
		else{
			glDisable(GL_LIGHT0);
		}
		break;
	case '2':
		if (!glIsEnabled(GL_LIGHT1)){
			glEnable(GL_LIGHT1);
		}
		else{
			glDisable(GL_LIGHT1);
		}
		break;
	case '3':
		if (!glIsEnabled(GL_LIGHTING)){
			glEnable(GL_LIGHTING);
		}
		else{
			glDisable(GL_LIGHTING);
		}
		break;
	case 'm':
		isWaveMotion = !isWaveMotion;
	default:
		printf("MOUSE!?\n");
		break;
	}
}// end routine keyboardCallback

void Window::specKeyboardCallback(int key, int x, int y){
	switch (key){
	case GLUT_KEY_F1:
//		mtx.changeChild(m);
		mat_mode = 0;
		break;
	case GLUT_KEY_F2:
//		mtx.changeChild(m2);
		mat_mode = 1;
		break;
	case GLUT_KEY_F3:
//		mtx.changeChild(m3);
		mat_mode = 2;
		break;
	case GLUT_KEY_F4:
		break;
	case GLUT_KEY_F5:
		break;
	default:
		break;
	}
}// end routine specKeyboardCallback

enum Movement {ROTATE, ZOOM, NONE};
Movement MOVEMENT;

Vector3 lastPoint;

void Window::mouseCallback(int button, int state, int x, int y){
	switch (button){
	case GLUT_LEFT_BUTTON:
		if (state == GLUT_DOWN){
			printf("LEFT pressed \n");
			MOVEMENT = Movement::ROTATE;
			lastPoint = trackballMapping(Vector3(x, y, 0));
			glMatrixMode(GL_MODELVIEW);
		}
		else if(state == GLUT_UP){
			printf("LEFT released \n");
			MOVEMENT = Movement::NONE;
		}
		break;
	case GLUT_MIDDLE_BUTTON:
		break;
	case GLUT_RIGHT_BUTTON:
		if (state == GLUT_DOWN){
			printf("RIGHT pressed \n");
			MOVEMENT = Movement::ZOOM;
			lastPoint[X] = x;
			lastPoint[Y] = y;
			glMatrixMode(GL_PROJECTION);
		}
		else if (state == GLUT_UP){
			printf("RIGHT released \n");
			MOVEMENT = Movement::NONE;
		}
		break;
	default:
		break;
	}
}// end routine mouseCallback

void Window::motionHandler(int x, int y){
//	printf("%d, %d\n", x, y);
	//Matrix4 * c2wm = &(Globals::c2w.getMatrix());
	//Matrix4 * c2wm = &mtx.getRotationMT();
	Matrix4 *c2wm = &testMatrix;

	Vector3 direction;
	double pixelDiff;
	double rotAngle, zoomFactor;
	Vector3 currPoint;

	switch (MOVEMENT){
	case ROTATE:{
		currPoint = trackballMapping(Vector3(x, y, 0));
		direction = currPoint - lastPoint;
		double velocity = direction.length();
		if (velocity > 0.0001){
			//printf("Velocity: %f\n", velocity);
			Vector3 rotationAxis;
			//lastPoint.print("Last Point p: ");
			//currPoint.print("Curr Point p: ");
			rotationAxis = Vector3::cross(lastPoint, currPoint);
			rotationAxis.normalize();
			//rotationAxis.print("Rotation Axis");
			rotAngle = velocity * 90;

			//Matrix4 * mtx = &m.getMatrix();
			Matrix4 * mtx = c2wm;
//			mtx->print("Original...");
			Matrix4 temp;
			temp.identity();
//			temp.print("Before applying rotation...");
			//printf("Rotation Angle: %f\n", rotAngle);
			temp.makeRotate(rotAngle, rotationAxis);
//			temp.print("After applying rotation");
			*mtx = *mtx * temp;
	//		mtx->print("change...?");
			//displayCallback();
		}
		break;
	}
	case ZOOM:
		pixelDiff = x - lastPoint[X];
		zoomFactor = 1.0 + pixelDiff * 0.00008;
		c2wm->makeScale_f(zoomFactor, zoomFactor, zoomFactor);

		//currPoint[X] = x;
		//currPoint[Y] = y;
		//currPoint[Z] = 0;
		currPoint = lastPoint;
		currPoint.print("scaling");
		break;
	default:
		break;
	}
	lastPoint = currPoint;
}// end routine motionHandler

Vector3 Window::trackballMapping(Vector3 point){
	Vector3 v;  //
	double d;   //
	v[X] = (2.0 * point[X] - width) / width;
	v[Y] = (height - 2.0 * point[Y]) / height;
	v[Z] = 0.0;

	d = v.length();
	d = (d < 1.0) ? d : 1.0;
	v[Z] = sqrt(1.001 - d * d);
	v.normalize();
	return v;
}// end routine trackballMapping

void Window::initializeShader(int & id, const char filename[], size_t size){
	// ---assign a fresh id number for the shader in the program
	// --- --- could be optimized by storing the id number
	// -- uniqueness testing
	id = -1; // make a default value
	for (int i = 0; i < 10; i++){
		if (isShaderPosFree[i] == false){
			isShaderPosFree[i] = true; // indicate that we're now using a shader 
				id = i;
			break; // we're done here
		}
	}
	if (id == -1){
		cerr << "WARNING: Out of free shaders, increase shader amount to use more." << endl;
		cerr << "WARNING: Shader " << filename << " is not loaded!" << endl;
		return;
	}

	char * vert = (char *)malloc(size + 6);
	char vert_ext[6] = ".vert";
	strcpy_s(vert, size + 6, filename);
	strcat(vert, vert_ext);

	char * frag = (char *)malloc(size + 6);
	char frag_ext[6] = ".frag";
	strcpy_s(frag, size + 6, filename);
	strcat(frag, frag_ext);

	cout << frag << endl;
	shadersArr[id] = *new Shader(vert, frag, true);

	//--- free up all the pointers
	free(vert);
	free(frag);
}// end routine initializeShader


/** Load a ppm file from disk.
@input filename The location of the PPM file.  If the file is not found, an error message
will be printed and this function will return 0
@input width This will be modified to contain the width of the loaded image, or 0 if file not found
@input height This will be modified to contain the height of the loaded image, or 0 if file not found
@return Returns the RGB pixel data as interleaved unsigned chars (R0 G0 B0 R1 G1 B1 R2 G2 B2 .... etc) or 0 if an error ocured
**/
unsigned char* loadPPM(const char* filename, int& width, int& height)
{
	const int BUFSIZE = 128;
	FILE* fp;
	unsigned int read;
	unsigned char* rawData;
	char buf[3][BUFSIZE];
	char* retval_fgets;
	size_t retval_sscanf;

	if ((fp = fopen(filename, "rb")) == NULL)
	{
		std::cerr << "error reading ppm file, could not locate " << filename << std::endl;
		width = 0;
		height = 0;
		return NULL;
	}

	// Read magic number:
	retval_fgets = fgets(buf[0], BUFSIZE, fp);

	// Read width and height:
	do
	{
		retval_fgets = fgets(buf[0], BUFSIZE, fp);
	} while (buf[0][0] == '#');
	retval_sscanf = sscanf(buf[0], "%s %s", buf[1], buf[2]);
	width = atoi(buf[1]);
	height = atoi(buf[2]);

	// Read maxval:
	do
	{
		retval_fgets = fgets(buf[0], BUFSIZE, fp);
	} while (buf[0][0] == '#');

	// Read image data:
	rawData = new unsigned char[width * height * 3];
	read = fread(rawData, width * height * 3, 1, fp);
	fclose(fp);
	if (read != 1)
	{
		std::cerr << "error parsing ppm file, incomplete data" << std::endl;
		delete[] rawData;
		width = 0;
		height = 0;
		return NULL;
	}

	return rawData;
}

// load image file into texture object
void loadTexture(const char * path, GLuint texture)
{
	//GLuint texture[1];     // storage for one texture
	int twidth, theight;   // texture width/height [pixels]
	unsigned char* tdata;  // texture pixel data

	// Load image file
	tdata = loadPPM(path, twidth, theight);
	if (tdata == NULL) return;

	// Set this texture to be the one we are working with
	glBindTexture(GL_TEXTURE_2D, texture);

	// Make sure no bytes are padded:	// Select GL_MODULATE to mix texture with polygon color for shading:
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	// Set bi-linear filtering for both minification and magnification
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	// Generate the texture
	glTexImage2D(GL_TEXTURE_2D, 0, 3, twidth, theight, 0, GL_RGB, GL_UNSIGNED_BYTE, tdata);
}


//--------

//--- EOD ---//