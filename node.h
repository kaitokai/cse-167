#ifndef _NODE_H_
#define _NODE_H_

#include "Matrix4.h"
#include <GL/glut.h>

class Node{
private:
public:
	Vector4 center;
	Vector4 point;
	double radius;
	virtual void draw(Matrix4 C) = 0;
	virtual void update() = 0;
	void cull(){
		//double width = 2 * 1 * tan(30);
		//double height = width * aspect;
	}
};

#endif