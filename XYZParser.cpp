#include "XYZParser.h"
#include <ctime>

//--- constructors
XYZParser::XYZParser(){
	isInitialized = false;
	m2w.identity();
	setMinMaxArr();
}

XYZParser::~XYZParser(){
	delete[] min;
	delete[] max;
	delete[] cntr;
}// end destructor

void XYZParser::setMinMaxArr(){
	for (int i = 0; i < 3; i++){
		min[i] = DBL_MAX;    //--- finds the minimum
		max[i] = -DBL_MAX; //--- finds the maximum
	}
}

void XYZParser::checkForMinMax(double x, double y, double z){
	min[X] = fmin(x, min[X]);
	max[X] = fmax(x, max[X]);

	min[Y] = fmin(y, min[Y]);
	max[Y] = fmax(y, max[Y]);

	min[Z] = fmin(z, min[Z]);
	max[Z] = fmax(z, max[Z]);

	cntr[X] = (min[X] + max[X]) / 2;
	cntr[Y] = (min[Y] + max[Y]) / 2;
	cntr[Z] = (min[Z] + max[Z]) / 2;

	int x_len = (max[X] - min[X]);
	//--- factor at which we scale the picture to
	if (x_len != 0){
		scFact = ((40 * tan(30 * M_PI / 180))) / x_len;
	}
}

void XYZParser::parse(const char * fname, const char type){
	switch (type){
	case 'p':
		this->parse(fname);
		break;
	case 'o':{
		double time = clock();
		FILE * f = fopen(fname, "rb");
		vector<Vector4> verts;
		vector<Vector3> norms;
		vector<Vector3> colrs;

		Vector4 point;
		Vector3 colr, nrml;
//		Vertex * vert;
		Triangle * tri;
		if (f == NULL){
			cerr << "Error reading file." << endl;
			exit(-1);
		}
		char c, c2;
		int line = 1;
		do{
			c = fgetc(f);
			c2 = fgetc(f);
			//printf("Reading line: %d, read: %c %c\n", line++, c, c2);
			//			printf("%d\n", c + c2);
			switch (c + c2){
			case 'v' + ' ': // read the vertex information
				double x, y, z;
				double r, g, b;
				fscanf_s(f, "%lf %lf %lf %lf %lf %lf\n", &x, &y, &z, &r, &g, &b);
				checkForMinMax(x, y, z); // 

				point = Vector4(x, y, z, 1); // create the point
				colr = Vector3(r, g, b);     // create color vector

				//--- store it for later triangle compliation
				verts.push_back(point);
				colrs.push_back(colr);
				break;
			case 'v' + 'n': // read the normal information
				//--- check to see that point and color are not NULL
				double nx, ny, nz;
				fscanf_s(f, "%lf %lf %lf\n", &nx, &ny, &nz);
				nrml = Vector3(nx, ny, nz);
				//nrml->normalize(); // pre-normalize the normalization

				//--- shove the normal into the normal vector
				norms.push_back(nrml);
				break;
			case 'f' + ' ': {
				// add the stuff to the things
				//--- It's time to build the triangle
				int _pos[3];
				int _nrm[3];
				fscanf_s(f, "%d//%d %d//%d %d//%d\n", &_pos[0], &_nrm[0],
					&_pos[1], &_nrm[1],
					&_pos[2], &_nrm[2]);

				Vertex _verks[3];
				for (int i = 0; i < 3; i++){
					if (i < 3){
						_verks[i] = Vertex(verts[_pos[i] - 1], norms[_nrm[i] - 1], colrs[_pos[i] - 1]); // minus one
					}
				}


				tri = new Triangle(_verks);
				//--- add the triangle to the list for later drawing
				vert_x.push_back(*tri);
				break;
			}
			default:
				while (true){
					c = fgetc(f);
					if (c == EOF || c == '\n'){
						cout << "SPIN ON LINE NUMBER: " << line << endl;
						break;
					}
				}
				break;
			}
			line++;
		} while (c != EOF);
		cout << "Finished parsing sequence." << endl;

		//--- we've finished reading the file
		fclose(f); // close the file

		//--- it's time to center and scale our world matrix
		centerAndScale();
		time = (double(clock()) - double(time)) / CLOCKS_PER_SEC;
		cout << "MIN AND MAX VALUES FOR X, Y, Z COMPONENTS" << endl;
		cout << "MIN X: " << min[X] << ", MAX X: " << max[X] << endl;
		cout << "MIN Y: " << min[Y] << ", MAX Y: " << max[Y] << endl;
		cout << "MIN Z: " << min[Z] << ", MAX Z: " << max[Z] << endl;
		cout << "SCALING FACTOR: " << scFact << endl;
		cout << "Number of vertices: " << verts.size() << endl;
		cout << "Number of faces   : " << vert_x.size() << endl;
		cout << "Total time for parse: " << time << " seconds." << endl;
		break;
	}
	default:
		cerr << "Unknown type" << endl;
		break;
	}
}

void XYZParser::parse(const char * fname){
	if (!isInitialized){
		double x = 0, y = 0, z = 0;
		double nx = 0, ny = 0, nz = 0;

		int totalSize = 0;
		FILE * f = fopen(fname, "r");
		if (f == NULL){
			cerr << "File does not exist!" << endl;
		}
		else{
			while (true){
				int chk = fscanf_s(f, "%lf %lf %lf %lf %lf %lf", &x, &y, &z, &nx, &ny, &nz);
				totalSize++;
				if (chk == EOF) break;
			}//
			//cout << totalSize << endl;
			verts.reserve(totalSize);
			norms.reserve(totalSize);
			colrs.reserve(totalSize);
			rewind(f);
			while (true){
				int chk = fscanf_s(f, "%lf %lf %lf %lf %lf %lf", &x, &y, &z, &nx, &ny, &nz);
				checkForMinMax(x, y, z);
				verts.push_back(*(new Vector4(x, y, z, 1)));
				Vector3 * m = new Vector3(nx, ny, nz);
				m->normalize();
				norms.push_back(*m);

				if (chk == EOF) break;
			}
		}//

		//
		cout << "MIN AND MAX VALUES FOR X, Y, Z COMPONENTS" << endl;
		cout << "MIN X: " << min[X] << ", MAX X: " << max[X] << endl;
		cout << "MIN Y: " << min[Y] << ", MAX Y: " << max[Y] << endl;
		cout << "MIN Z: " << min[Z] << ", MAX Z: " << max[Z] << endl;

		//--- finding the center value of the rectanguloid
		centerAndScale();
		fclose(f);
		isInitialized = true;
	}
}// end routine parse

void XYZParser::parse(double * verts, double * colrs, int size){
	if (!isInitialized){
		_ASSERT(size % 3 == 0);
		for (int i = 0; i < size; i += 3){
			this->verts.push_back(*(new Vector4(verts[i], verts[i + 1], verts[i + 2], 1)));
			//			this->colrs.push_back(*(new Vector3(colrs[i], colrs[i + 1], colrs[i + 2])));
			checkForMinMax(verts[i], verts[i + 1], verts[i + 2]);
		}
		m2w.identity();
		m2w_init = m2w;
		isInitialized = true;
	}
}// end routine parse

void XYZParser::draw(Matrix4 C){
	Geode::draw(C);
	render();
}

void XYZParser::render(){
	glBegin(GL_TRIANGLES);
	for (unsigned int i = 0; i < vert_x.size(); i++){

		for (int j = 0; j < 3; j++){
			Vertex _vrt = vert_x[i][j];
			Vector3 _clr = _vrt.getColors();
			Vector3 _nrm = _vrt.getNormals();
			Vector4 _pos = _vrt.getPos();

			glColor3d(1, 1, 1);
			glNormal3d(_nrm.x(), _nrm.y(), _nrm.z());
			glVertex3d(_pos.x(), _pos.y(), _pos.z());
		}

	}
	glEnd();
}

void XYZParser::update(){

}

const Vector4 XYZParser::getVerts(int i) {
	return verts[i];
}// end routine getVerts

Vector3 XYZParser::getNorms(int i){
	return norms[i];
}// end routine getNorms

int XYZParser::count(){
	return verts.size();
}// end routine count

Matrix4& XYZParser::getMatrix(){
	return m2w;
}// end routine getMatrix

void XYZParser::reset(){
	m2w = m2w_init;
}// end routine reset

bool XYZParser::isInit() const{
	return isInitialized;
}// end routine isInit
