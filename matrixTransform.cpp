#include "matrixTransform.h"

MatrixTransform::MatrixTransform(Matrix4 M) : M(M){};

void MatrixTransform::draw(Matrix4 C){
	Group::draw(C * M);
}

void MatrixTransform::update(){
	for (list<Node*>::iterator iter = children.begin(); iter != children.end(); iter++){
		(*iter)->update();
		center = M * (*iter)->center;
		point = M * (*iter)->point;
		radius = ((*iter)->center - (*iter)->point).length();
	}
}

//--- EOD