#include "cubic.h"

Cubic::Cubic():Cubic(1){}

Cubic::Cubic(double size):size(size){
}

void Cubic::draw(Matrix4 C){
	//---
	Geode::draw(C);
	render();
}

void Cubic::update(){
	radius = sqrt(3*size)/2;
	Vector3 _r(0, radius, 0), _r2(radius, 0, 0), _r3(0, 0, radius);
	Vector3 _center = center.toVector3();
	double radius1 = (_center - _r).length(), 
		   radius2 = (_center - _r2).length(), 
		   radius3 = (_center - _r3).length();
	if (radius1 > radius2 && radius1 > radius3){
		radius = radius1;
	}
	else if (radius2 > radius1 && radius2 > radius3){
		radius = radius2;
	}
	else if (radius3 > radius1 && radius3 > radius2){
		radius = radius3;
	}
	point = center + Vector4(radius, radius, radius, 0);

}

void Cubic::render(){
	glutSolidCube(size);
	//glutWireSphere(radius, 15, 15);
}// end routine

//--- EOD