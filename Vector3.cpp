#include <math.h>
#include <iostream>

#include "Vector3.h"

Vector3::Vector3() : Vector3(0, 0, 0) {}

Vector3::Vector3(float x, float y, float z)
{
	v[0] = x;
	v[1] = y;
	v[2] = z;
} 
Vector3::~Vector3(){
	//delete [] v;
}

void Vector3::print(string comment) const
{
	cout << comment << "< " << v[0] << ", " << v[1] << ", " << v[2] << " >" << endl;
}

string Vector3::toString() const
{
	return "< " + to_string(v[0]) + ", " + to_string(v[1]) + ", " + to_string(v[2]) + " >";
}

Vector3 Vector3::operator+(const Vector3& other)
{
	float x = v[0] + other.v[0];
	float y = v[1] + other.v[1];
	float z = v[2] + other.v[2];

	return Vector3(x, y, z);
}


Vector3 Vector3::operator-(const Vector3& other)
{
	float x = v[0] - other.v[0];
	float y = v[1] - other.v[1];
	float z = v[2] - other.v[2];

	return Vector3(x, y, z);
}

Vector3 Vector3::operator/(float s){
	Vector3 temp = *this;
	if (s != 0){
		temp.scale(1 / s);
	}
	return temp;
}

Vector3 Vector3::operator*(const Vector3& other){
	Vector3 temp;
	for (int i = 0; i < 3; i++){
		temp.v[i] = v[i] * other.v[i];
	}
	return temp;
}

Vector3 Vector3::operator=(const Vector3& other){
	if (this != &other){
		this->v[0] = other.v[0];
		this->v[1] = other.v[1];
		this->v[2] = other.v[2];
	}
	return *this;
}

bool Vector3::operator==(const Vector3 & other){
	if (this == &other){
		return true;
	}

	return other.x() == this->x() && 
		     other.y() == this->y() && 
				 other.z() == this->z();
}

float& Vector3::operator[](int i){
	return v[i];
}

void Vector3::negate()
{
	v[0] = -v[0];
	v[1] = -v[1];
	v[2] = -v[2];
}

Vector3 Vector3::scale(float s)
{
	v[0] *= s;
	v[1] *= s;
	v[2] *= s;
	return *this;
}

float Vector3::dot(const Vector3& a, const Vector3& b)
{
	return (a.v[0] * b.v[0]) + (a.v[1] * b.v[1]) + (a.v[2] * b.v[2]);
}

float Vector3::dot(const Vector3& other)
{
	return (v[0] * other.v[0]) + (v[1] * other.v[1]) + (v[2] * other.v[2]);
}

Vector3 Vector3::cross(const Vector3& a, const Vector3& b)
{
	float x = (a.v[1] * b.v[2]) - (a.v[2] * b.v[1]);
	printf("");
	float y = (a.v[2] * b.v[0]) - (a.v[0] * b.v[2]);
	float z = (a.v[0] * b.v[1]) - (a.v[1] * b.v[0]);

	return Vector3(x, y, z);
}

Vector3 Vector3::cross(const Vector3& other){
	return cross(*this, other);
}

float Vector3::length()
{
	float sumSquares = (v[0] * v[0]) + (v[1] * v[1]) + (v[2] * v[2]);
	return sqrt(sumSquares);
}

void Vector3::normalize()
{
	float mag = length();
	v[0] /= mag;
	v[1] /= mag;
	v[2] /= mag;
}

float Vector3::x() const
{
	return v[0];
}

float Vector3::y() const
{
	return v[1];
}

float Vector3::z() const
{
	return v[2];
}

float * Vector3::getPointer(){
	return &v[0];
}

Vector4 Vector3::toVector4(int type){
	_ASSERT(type <= 1 && 0 <= type);
	return Vector4(v[0], v[1], v[2], type);
}
